package com.mtech.customerSide;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CartDao {


    @Insert
    public void insertAll(Cart cart);

    @Query("SELECT * FROM MyCart")
    public List<Cart> getAllCartItem();

    @Query("SELECT EXISTS (SELECT 1 FROM MyCart WHERE id=:id)")
    public int isAddToCart(int id);

    @Query("select COUNT (*) from MyCart")
    int countCart();

    @Query("DELETE FROM MyCart WHERE id=:id")
    public void deleteItem(int id);

    @Query("DELETE FROM MyCart")
    public void deleteAllItem();

}
