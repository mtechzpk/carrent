package com.mtech.customerSide.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.customerSide.GpsLocation.GPSTracker;
import com.mtech.customerSide.R;
import com.mtech.customerSide.SMS.Database;
import com.mtech.customerSide.model.UserResponseModel;
import com.mtech.customerSide.networks.GetDateService;
import com.mtech.customerSide.networks.RetrofitClientInstance;
import com.mtech.customerSide.notifications.Notifications;
import com.mtech.customerSide.utils.Utilities;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    KProgressHUD dialog0;
    private Button btn_signin;
    private EditText ed_Phone;
    private ShowHidePasswordEditText Ed_Password;
    String latitude, longitude;
    TextView forgotPassword;
    CheckBox checkbox;
    String newToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        forgotPassword = findViewById(R.id.forgotPassword);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LoginActivity.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Utilities.saveString(LoginActivity.this, "device_token", newToken);
            }
        });

        GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.getIsGPSTrackingEnabled()) {
            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());
            Utilities.saveString(LoginActivity.this, "current_lat", latitude);
            Utilities.saveString(LoginActivity.this, "current_lng", longitude);
            Toast.makeText(this, "" + Utilities.getString(this, "current_lat") + Utilities.getString(this, "current_lng"), Toast.LENGTH_SHORT).show();

//            Toast.makeText(HomeActivity.this, latitude + " " + longitude, Toast.LENGTH_SHORT).show();
        } else {
            gpsTracker.showSettingsAlert();
        }
        btn_signin = findViewById(R.id.btn_signin);
        ed_Phone = findViewById(R.id.ed_Phone);
        Ed_Password = findViewById(R.id.ed_password);
        checkbox = findViewById(R.id.tv_termsandconditions);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,ForgotPassActivity.class));
            }
        });
        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String getPhone = ed_Phone.getText().toString();
                String password = Ed_Password.getText().toString();

                if (getPhone.isEmpty() || password.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Please Add Missing Fields", Toast.LENGTH_SHORT).show();
                } else if (!checkbox.isChecked()) {
                    Toast.makeText(LoginActivity.this, "For continue, you have to agree to accept our Privacy Policy & Terms of Service.", Toast.LENGTH_SHORT).show();
                } else {
                    userLogin(getPhone, password);
                }

            }
        });
    }

    private void userLogin(String phone, String password) {
        dialog0 = KProgressHUD.create(LoginActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<UserResponseModel> call = service.loginUser(phone, password, newToken);

        call.enqueue(new Callback<UserResponseModel>() {
            @Override
            public void onResponse(Call<UserResponseModel> call, Response<UserResponseModel> response) {
                dialog0.dismiss();

                assert response.body() != null;
                int status = response.body().getStatus();
                if (status == 200) {
//                    Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    dialog0.dismiss();
                    String name = response.body().getData().getName();

                    int userID = response.body().getData().getId();
                    String userEmail = response.body().getData().getEmail();
                    String userPhone = response.body().getData().getPhone();
                    String profileImage = response.body().getData().getProfile_image();
                    String support_email = response.body().getData().getSupport_email();
                    String support_phone = response.body().getData().getSupport_phone();
                    String sec_deposit = response.body().getData().getSecurity_deposite();
                    String sec_deposit_currency = response.body().getData().getSecurity_deposite_currency();

                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                    Utilities.saveString(LoginActivity.this, "login_status", "customer");

                    Utilities.saveString(LoginActivity.this, Utilities.USER_FNAME, name);
                    Utilities.saveString(LoginActivity.this, Utilities.USER_ID, String.valueOf(userID));
                    Utilities.saveString(getApplicationContext(), "userEmail", userEmail);
                    Utilities.saveString(getApplicationContext(), "userPhone", userPhone);
                    Utilities.saveString(getApplicationContext(), "userName", name);
                    Utilities.saveString(getApplicationContext(), "support_phone", support_phone);
                    Utilities.saveString(getApplicationContext(), "support_email", support_email);
                    Utilities.saveString(LoginActivity.this, "mobile", ed_Phone.getText().toString());
                    Utilities.saveString(getApplicationContext(), "security_deposite",sec_deposit);
                    Utilities.saveString(getApplicationContext(), "security_deposite_currency",sec_deposit_currency);


                    Utilities.saveString(getApplicationContext(), Utilities.USER_PROFILE_IMAGE, profileImage);

                    startActivity(intent);
                    finish();
                    FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
                        @Override
                        public void onComplete(@NonNull Task<String> task) {

                            if (task.isSuccessful()) {

                                String token = task.getResult();
                                Utilities.saveString(LoginActivity.this, Utilities.USER_TOKEN, token);
                                FirebaseDatabase.getInstance().getReference(Database.TOKENS)
                                        .child(String.valueOf(userID)).setValue(token);

                                Notifications.createNotificationChannel(LoginActivity.this);
                            }
                        }
                    });

                    finish();

                } else {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<UserResponseModel> call, Throwable t) {
                dialog0.dismiss();
                t.printStackTrace();

            }
        });

    }

    public void SignUp(View v) {

        startActivity(new Intent(LoginActivity.this, SignUpActivity.class));

    }

    public void terms(View view) {

        String url = "http://yuunri.com/terms_of_use";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}