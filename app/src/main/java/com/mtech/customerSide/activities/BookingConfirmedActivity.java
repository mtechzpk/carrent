package com.mtech.customerSide.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import com.android.volley.Request;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.customerSide.Cart;
import com.mtech.customerSide.MyDatabase;
import com.mtech.customerSide.R;
import com.mtech.customerSide.api.ApiModelClass;
import com.mtech.customerSide.api.ServerCallback;
import com.mtech.customerSide.model.CommonResponseModel;
import com.mtech.customerSide.networks.GetDateService;
import com.mtech.customerSide.networks.RetrofitClientInstance;
import com.mtech.customerSide.utils.Utilities;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingConfirmedActivity extends AppCompatActivity {
    TextView tv_carName, tv_price, tv_priceperhour, tv_pickuplocation, tv_description,
            tv_pickupdatetime, tv_returnlocation, tvSecurityDeposit, tvPriceperHour, tvVat, tvTotal, tv_returndatetime, tvSubtotal, tvLocationprice, tv_paymentmethod, tv_cancel, tvRentalFee, tvRentalDuration, tvPriceperDay;
    ImageView img_backarrow, img_picture;
    Button bDoneButtomPayment;
    LinearLayout llpricePerHour;
    String userID = "", vehicle_id = "", vehicle_image = "", vehicle_NamewithColor = "", userPhone = "", userName = "", carDescription = "", vehicle_color = "", vehicle_Name = "", carRate = "", currency = "";
    String bookPickDateTime = "", returnkuDateTime = "", getPickupAddress = "", pickupLatitude = "", pickupLongitude = "", getReturnAddresss = "",
            returnLatitude = "", carRateperDay = "", returnLongitude = "", onlycarRate = "", drivingLiscenseImage = "", _paymentMethode = "", customerName = "", customerPhone = "", customerAddres = "";
    KProgressHUD hud;
    Double finalprice;
    String finalPriceWithVat = "";
    String rateWIthcUrrency = "";
    String rental_duration = "";
    String hoursText = "";
    String timeDuration = "", security_deposite = "", security_deposite_currency = "";
    Double totalFee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_confirmed);

        userID = Utilities.getString(BookingConfirmedActivity.this, Utilities.USER_ID);
        vehicle_id = Utilities.getString(BookingConfirmedActivity.this, "vehicle_id");
        userPhone = Utilities.getString(BookingConfirmedActivity.this, "userPhone");
        vehicle_image = Utilities.getString(BookingConfirmedActivity.this, "vehicle_image");
        userName = Utilities.getString(BookingConfirmedActivity.this, "userName");
        drivingLiscenseImage = Utilities.getString(BookingConfirmedActivity.this, "drivingLiscenseImage");
        carDescription = Utilities.getString(BookingConfirmedActivity.this, "carDescription");
        vehicle_color = Utilities.getString(BookingConfirmedActivity.this, "vehicle_color");
        vehicle_Name = Utilities.getString(BookingConfirmedActivity.this, "vehicle_Name");
        carRate = Utilities.getString(BookingConfirmedActivity.this, "carRateWithCurrency");
        currency = Utilities.getString(BookingConfirmedActivity.this, "currency");
        onlycarRate = Utilities.getString(BookingConfirmedActivity.this, "carRate");
        carRateperDay = Utilities.getString(BookingConfirmedActivity.this, "carRateperDay");
        rateWIthcUrrency = currency + carRateperDay + "/Day";
        bookPickDateTime = Utilities.getString(BookingConfirmedActivity.this, "bookPickDateTime");
        returnkuDateTime = Utilities.getString(BookingConfirmedActivity.this, "returnkuDateTime");
        getPickupAddress = Utilities.getString(BookingConfirmedActivity.this, "getPickupAddress");
        pickupLatitude = Utilities.getString(BookingConfirmedActivity.this, "pickupLatitude");
        pickupLongitude = Utilities.getString(BookingConfirmedActivity.this, "pickupLongitude");
        getReturnAddresss = Utilities.getString(BookingConfirmedActivity.this, "getReturnAddresss");
        returnLatitude = Utilities.getString(BookingConfirmedActivity.this, "returnLatitude");
        _paymentMethode = Utilities.getString(BookingConfirmedActivity.this, "_paymentMethode");
        customerName = Utilities.getString(BookingConfirmedActivity.this, "customerName");
        customerPhone = Utilities.getString(BookingConfirmedActivity.this, "customerPhone");
        customerAddres = Utilities.getString(BookingConfirmedActivity.this, "customerAddres");
        returnLongitude = Utilities.getString(BookingConfirmedActivity.this, "returnLongitude");
        rental_duration = Utilities.getString(BookingConfirmedActivity.this, "rental_duration");
        timeDuration = Utilities.getString(BookingConfirmedActivity.this, "timeDuration");
        security_deposite = Utilities.getString(BookingConfirmedActivity.this, "security_deposite");
        security_deposite_currency = Utilities.getString(BookingConfirmedActivity.this, "security_deposite_currency");
        if (timeDuration.equals("1")) {
            hoursText = "Hour";
        } else {
            hoursText = "Hours";

        }
        vehicle_NamewithColor = vehicle_Name + " (" + vehicle_color + ") ";
        init();
        bDoneButtomPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_paymentMethode.equals("Cash On Delivery")) {


                    bookApi(bookPickDateTime, returnkuDateTime, getPickupAddress, pickupLatitude, pickupLongitude, getReturnAddresss, returnLatitude, returnLongitude, _paymentMethode, customerName, customerPhone, customerAddres);
                } else {
                    Utilities.saveString(BookingConfirmedActivity.this, "finalPriceWithVat", String.valueOf(new DecimalFormat("##").format(totalFee)));
                    startActivity(new Intent(BookingConfirmedActivity.this, PayPallActivity.class));
                }

            }
        });
    }

    private void init() {
        img_backarrow = findViewById(R.id.img_backarrow);
        bDoneButtomPayment = findViewById(R.id.bDoneButtomPayment);
        tvTotal = findViewById(R.id.tvTotal);
        tvRentalFee = findViewById(R.id.tvRentalFee);
        tvPriceperDay = findViewById(R.id.tvPriceperDay);
        tvSecurityDeposit = findViewById(R.id.tvSecurityDeposit);
        tvRentalDuration = findViewById(R.id.tvRentalDuration);
        img_picture = findViewById(R.id.img_picture);
        tv_carName = findViewById(R.id.tv_carName);
        tv_cancel = findViewById(R.id.tv_cancel);
        tv_pickuplocation = findViewById(R.id.tv_pickuplocation);
        tv_priceperhour = findViewById(R.id.tv_priceperhour);
        tv_price = findViewById(R.id.tv_price);
        tv_returnlocation = findViewById(R.id.tv_returnlocation);
        tv_returndatetime = findViewById(R.id.tv_returndatetime);
        tv_description = findViewById(R.id.tv_description);
        tv_paymentmethod = findViewById(R.id.tv_paymentmethod);
        tvSubtotal = findViewById(R.id.tvSubtotal);
        tvLocationprice = findViewById(R.id.tvLocationprice);
        tvPriceperHour = findViewById(R.id.tvPriceperHour);
        tv_pickupdatetime = findViewById(R.id.tv_pickupdatetime);
        llpricePerHour = findViewById(R.id.llpricePerHour);
        tvVat = findViewById(R.id.tvVat);
        Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG + vehicle_image).into(img_picture);
        tv_carName.setText(vehicle_Name + " (" + vehicle_color + ")");
        tv_pickuplocation.setText(getPickupAddress);
        tv_returnlocation.setText(getReturnAddresss);
        tv_returndatetime.setText(returnkuDateTime);
        tv_pickupdatetime.setText(bookPickDateTime);
        tvPriceperHour.setText(currency + onlycarRate);
        tv_description.setText(carDescription);

        if (timeDuration.equals("0")) {
            tvRentalDuration.setText(rental_duration + " Day ");
            llpricePerHour.setVisibility(View.GONE);
        } else {
            tvRentalDuration.setText(rental_duration + " Day " + timeDuration + " " + hoursText);

        }
        tv_priceperhour.setText(currency + carRateperDay + "/Day");
        tvPriceperDay.setText(currency + carRateperDay);
        Double vat = 0.12 * Double.parseDouble(onlycarRate);
        finalprice = vat + Double.parseDouble(onlycarRate);
        finalPriceWithVat = currency + new DecimalFormat("##.##").format(finalprice);
        tv_price.setText(finalPriceWithVat);
        tv_paymentmethod.setText(_paymentMethode);
        Double rentalFee = Double.parseDouble(carRateperDay) * Double.parseDouble(rental_duration);
        Double hourRate = Double.parseDouble(timeDuration) * Double.parseDouble(onlycarRate);
        Double totalDatHourRate = rentalFee + hourRate;
        tvRentalFee.setText(currency + String.valueOf(totalDatHourRate));
        tvSubtotal.setText(currency + String.valueOf(totalDatHourRate));
        Double vatwithTotal = 0.12 * totalDatHourRate;
        tvVat.setText(String.valueOf(vatwithTotal));
        totalFee = vatwithTotal + totalDatHourRate;
        tvTotal.setText(currency + String.valueOf(new DecimalFormat("##").format(totalFee)));
        tvSecurityDeposit.setText(security_deposite_currency + security_deposite);
        img_backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void bookCarApi(String pickup_date, String return_date, String pickup_location, String pickup_latitude,
                            String pickup_longitude, String return_location, String return_latitude,
                            String return_longitude, String payment_method, String userNameee, String userPone,
                            String addresss) {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();
        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<CommonResponseModel> call = service.book_vehicle(userID, vehicle_id, pickup_date, return_date, pickup_location, pickup_latitude, pickup_longitude, return_location, return_latitude, return_longitude, payment_method, userNameee, userPone, addresss, drivingLiscenseImage, rental_duration);
        call.enqueue(new Callback<CommonResponseModel>() {
            @Override
            public void onResponse(Call<CommonResponseModel> call, Response<CommonResponseModel> response) {

                assert response.body() != null;
                int status = response.body().getStatus();
                //String profileImage = response.body().getData().getProfile_image();
                if (status == 200) {
                    hud.dismiss();
                    if (_paymentMethode.equals("Cash On Delivery")) {

                        saveInvoice(vehicle_NamewithColor, rateWIthcUrrency, carDescription, getPickupAddress, getReturnAddresss, returnkuDateTime, bookPickDateTime, _paymentMethode);
                        Toast.makeText(BookingConfirmedActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(BookingConfirmedActivity.this, HomeActivity.class));
                        finish();
                    } else {
                        Toast.makeText(BookingConfirmedActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                        startActivity(new Intent(BookingConfirmedActivity.this, PayPallActivity.class));
                    }

                } else {
                    hud.dismiss();
                    Toast.makeText(BookingConfirmedActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<CommonResponseModel> call, Throwable t) {

                hud.dismiss();
                t.printStackTrace();
            }
        });
    }

    public void saveInvoice(String carNme, String carPrice, String description, String pickupLoc, String return_loc, String return_dateTime, String pickupDateTime, String paymentMethode) {
//        Toast.makeText(BookingConfirmedActivity.this, "Add to Invoice History Successfully", Toast.LENGTH_SHORT).show();
        MyDatabase database = Room.databaseBuilder(BookingConfirmedActivity.this, MyDatabase.class, "MyCart").allowMainThreadQueries().build();

        Cart cart = new Cart(carNme, carPrice, description, pickupLoc, return_loc, return_dateTime, pickupDateTime, paymentMethode);
//        Cart cart = new Cart(carNme,123,carDescription);
        database.cartDao().insertAll(cart);
    }


    public void bookApi(String pickup_date, String return_date, String pickup_location, String pickup_latitude,
                        String pickup_longitude, String return_location, String return_latitude,
                        String return_longitude, String payment_method, String userNameee, String userPone,
                        String addresss) {
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();
        Map<String, String> params = new HashMap<String, String>();
        params.put("customer_id", userID);
        params.put("vehicle_id", vehicle_id);
        params.put("pickup_date_and_time", pickup_date);
        params.put("return_date_and_time", return_date);
        params.put("pickup_location", pickup_location);
        params.put("pickup_latitude", pickup_latitude);
        params.put("pickup_longitude", pickup_longitude);
        params.put("return_location", return_location);
        params.put("return_latitude", return_latitude);
        params.put("return_longitude", return_longitude);
        params.put("payment_method", payment_method);

        params.put("customer_name", userNameee);
        params.put("customer_phone", customerPhone);
        params.put("customer_address", addresss);
        params.put("driving_license_image", drivingLiscenseImage);
        params.put("rental_duration", rental_duration);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Accept", "application/json");


        ApiModelClass.GetApiResponse(Request.Method.POST, RetrofitClientInstance.BASE_URL + "book_vehicle", BookingConfirmedActivity.this, params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    hud.dismiss();

                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(result));

                        int status = jsonObject.getInt("status");
                        String Message = jsonObject.getString("message");
                        if (status == 200) {

                            if (_paymentMethode.equals("Cash On Delivery")) {

                                saveInvoice(vehicle_NamewithColor, rateWIthcUrrency, carDescription, getPickupAddress, getReturnAddresss, returnkuDateTime, bookPickDateTime, _paymentMethode);
                                Toast.makeText(BookingConfirmedActivity.this, Message, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(BookingConfirmedActivity.this, HomeActivity.class));
                                finish();
                            } else {
//                                saveInvoice(vehicle_NamewithColor, rateWIthcUrrency, carDescription, getPickupAddress, getReturnAddresss, returnkuDateTime, bookPickDateTime, _paymentMethode);
                                Toast.makeText(BookingConfirmedActivity.this, Message, Toast.LENGTH_LONG).show();
//                        startActivity(new Intent(BookingConfirmedActivity.this, PayPallActivity.class));
                            }

                        } else {
                            hud.dismiss();
                            Toast.makeText(BookingConfirmedActivity.this, Message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    hud.dismiss();
                    Toast.makeText(BookingConfirmedActivity.this, ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }

}