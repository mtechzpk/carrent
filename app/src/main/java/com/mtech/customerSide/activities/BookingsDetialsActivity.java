package com.mtech.customerSide.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.borjabravo.readmoretextview.ReadMoreTextView;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.customerSide.R;
import com.mtech.customerSide.model.CommonResponseModel;
import com.mtech.customerSide.model.booking.AcceptedBookingDataModel;
import com.mtech.customerSide.model.booking.CurrentBookingDataModel;
import com.mtech.customerSide.model.booking.PastBookingDataModel;
import com.mtech.customerSide.networks.GetDateService;
import com.mtech.customerSide.networks.RetrofitClientInstance;
import com.mtech.customerSide.utils.Utilities;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingsDetialsActivity extends AppCompatActivity {
    public List<AcceptedBookingDataModel> acceptedBookingDataModels;
    ImageView img_backarrow, img_picture;
    ReadMoreTextView tv_description;
    public List<CurrentBookingDataModel> currentBookingDataModels;
    public List<PastBookingDataModel> pastBookingDataModels;
    TextView tv_carName, tv_status, tv_priceperhour, tv_pickuplocation,
            tv_pickupdatetime, tv_returnlocation, tv_returndatetime, tv_paymentmethod, tv_cancel, tvDuration;
    int position;
    Button bDoneButtomPayment;
    String bookType = "";
    String carRateperDay = "";
    String status = "";
    Double finalprice;
    KProgressHUD hud;
    int booking_request_id;
    String rental_duration = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookings_detials);
        init();

        position = getIntent().getIntExtra("position", -1);
        bookType = Utilities.getString(BookingsDetialsActivity.this, "bookType");
        carRateperDay = Utilities.getString(BookingsDetialsActivity.this, "carRateperDay");
        if (bookType.equals("pending")) {
            if (position != -1) {
                currentBookingDataModels = Utilities.getCurrentBookingModels(this);
                setData();
            }

        } else if (bookType.equals("past")) {
            if (position != -1) {
                pastBookingDataModels = Utilities.getPastBookingModels(this);

                setPastData();

            }
        } else if (bookType.equals("accepted")) {
            if (position != -1) {
                acceptedBookingDataModels = Utilities.getAcceptedBookingModels(this);
                setAcceptedData();

            }
        }

        Clicks();
    }

    private void setData() {
        bDoneButtomPayment.setVisibility(View.GONE);
        CurrentBookingDataModel model = currentBookingDataModels.get(position);
        booking_request_id = model.getId();
        Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG + currentBookingDataModels.get(position).getVehicle_image()).into(img_picture);
        tv_carName.setText(model.getVehicle_name());
        tv_status.setText(model.getStatus());
        tv_pickuplocation.setText(model.getPickup_location());
        tv_pickupdatetime.setText(model.getPickup_date_and_time());
        tv_returnlocation.setText(model.getReturn_location());
        tv_returndatetime.setText(model.getReturn_date_and_time());
        tv_paymentmethod.setText(model.getPayment_method());
        tv_description.setText(model.getVehicle_description());
        rental_duration = model.getRental_duration();
        if (rental_duration.equals("1")) {
            tvDuration.setText(rental_duration + " Day");

        } else {
            tvDuration.setText(rental_duration + " Days");

        }
        tv_priceperhour.setText(model.getPer_hour_rate_currency() + carRateperDay + "/Day");
        TextView tv_price = findViewById(R.id.tv_price);

        Double subtotal = Double.parseDouble(model.getRental_duration()) * Double.parseDouble(carRateperDay);
        Double vat = 0.12 * subtotal;
        finalprice = vat + subtotal;
        tv_price.setText(model.getPer_hour_rate_currency() + new DecimalFormat("##").format(finalprice));
        tv_pickuplocation.setSelected(true);
        tv_returndatetime.setSelected(true);
        tv_returnlocation.setSelected(true);
        tv_returnlocation.setSelected(true);

    }

    private void setPastData() {
        bDoneButtomPayment.setVisibility(View.GONE);
        PastBookingDataModel model = pastBookingDataModels.get(position);
        Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG + pastBookingDataModels.get(position).getVehicle_image()).into(img_picture);
        tv_carName.setText(model.getVehicle_name());
        booking_request_id = model.getId();
        tv_status.setText(model.getStatus());
        tv_pickuplocation.setText(model.getPickup_location());
        tv_pickupdatetime.setText(model.getPickup_date_and_time());
        tv_returnlocation.setText(model.getReturn_location());
        tv_returndatetime.setText(model.getReturn_date_and_time());
        tv_paymentmethod.setText(model.getPayment_method());
        tv_description.setText(model.getVehicle_description());
        rental_duration = model.getRental_duration();
        if (rental_duration.equals("1")) {
            tvDuration.setText(rental_duration + " Day");

        } else {
            tvDuration.setText(rental_duration + " Days");

        }
        tv_priceperhour.setText(model.getPer_hour_rate_currency() + carRateperDay + "/Day");
        TextView tv_price = findViewById(R.id.tv_price);

        Double subtotal = Double.parseDouble(model.getRental_duration()) * Double.parseDouble(carRateperDay);
        Double vat = 0.12 * subtotal;
        finalprice = vat + subtotal;
        tv_price.setText(model.getPer_hour_rate_currency() + new DecimalFormat("##.##").format(finalprice));
        tv_pickuplocation.setSelected(true);
        tv_returndatetime.setSelected(true);
        tv_returnlocation.setSelected(true);
    }

    private void setAcceptedData() {
        bDoneButtomPayment.setVisibility(View.VISIBLE);
        AcceptedBookingDataModel model = acceptedBookingDataModels.get(position);
        Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG + acceptedBookingDataModels.get(position).getVehicle_image()).into(img_picture);
        tv_carName.setText(model.getVehicle_name());
        booking_request_id = model.getId();
        tv_status.setText(model.getStatus());
        tv_pickuplocation.setText(model.getPickup_location());
        tv_pickupdatetime.setText(model.getPickup_date_and_time());
        tv_returnlocation.setText(model.getReturn_location());
        tv_returnlocation.setText(model.getReturn_location());
        tv_returndatetime.setText(model.getReturn_date_and_time());
        tv_paymentmethod.setText(model.getPayment_method());
        tv_description.setText(model.getVehicle_description());
        rental_duration = model.getRental_duration();
        if (rental_duration.equals("1")) {
            tvDuration.setText(rental_duration + " Day");

        } else {
            tvDuration.setText(rental_duration + " Days");

        }
        tv_priceperhour.setText(model.getPer_hour_rate_currency() + carRateperDay + "/Day");
        TextView tv_price = findViewById(R.id.tv_price);

        Double subtotal = Double.parseDouble(model.getRental_duration()) * Double.parseDouble(carRateperDay);
        Double vat = 0.12 * subtotal;
        finalprice = vat + subtotal;
        tv_price.setText(model.getPer_hour_rate_currency() + new DecimalFormat("##.##").format(finalprice));
        tv_pickuplocation.setSelected(true);
        tv_returndatetime.setSelected(true);
        tv_returnlocation.setSelected(true);
    }

    private void Clicks() {
        img_backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        bDoneButtomPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                cancelledBookingApi();
                showCancelledialogue();
            }
        });
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void init() {
        img_backarrow = findViewById(R.id.img_backarrow);
        tvDuration = findViewById(R.id.tvDuration);

        bDoneButtomPayment = findViewById(R.id.bDoneButtomPayment);
        img_picture = findViewById(R.id.img_picture);
        tv_carName = findViewById(R.id.tv_carName);
        tv_status = findViewById(R.id.tv_status);
        tv_cancel = findViewById(R.id.tv_cancel);
        tv_pickuplocation = findViewById(R.id.tv_pickuplocation);
        tv_priceperhour = findViewById(R.id.tv_priceperhour);
        tv_returnlocation = findViewById(R.id.tv_returnlocation);
        tv_returndatetime = findViewById(R.id.tv_returndatetime);
        tv_description = findViewById(R.id.tv_description);
        tv_paymentmethod = findViewById(R.id.tv_paymentmethod);
        tv_pickupdatetime = findViewById(R.id.tv_pickupdatetime);
    }

    private void cancelledBookingApi(String reasonText) {
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();
        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<CommonResponseModel> call = service.change_booking_status(String.valueOf(booking_request_id), "cancelled", reasonText);
        call.enqueue(new Callback<CommonResponseModel>() {
            @Override
            public void onResponse(Call<CommonResponseModel> call, Response<CommonResponseModel> response) {

                assert response.body() != null;
                int status = response.body().getStatus();
                //String profileImage = response.body().getData().getProfile_image();
                if (status == 200) {
                    hud.dismiss();
                    Toast.makeText(BookingsDetialsActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    hud.dismiss();
                    Toast.makeText(BookingsDetialsActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<CommonResponseModel> call, Throwable t) {

                hud.dismiss();
                t.printStackTrace();
            }
        });
    }


    private void showCancelledialogue() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        Dialog dialog;
        dialog = new Dialog(BookingsDetialsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cancelled_reason_dailog);
        dialog.getWindow().setLayout((int) (width * 0.8), (int) (height * 0.82)); //Controlling width and height.
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button bContinue = dialog.findViewById(R.id.bContinue);
        EditText edReasonText = dialog.findViewById(R.id.edReasonText);
        ImageView imgclose = dialog.findViewById(R.id.imgclose);

        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        bContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getText = edReasonText.getText().toString();
                if (!getText.equals("")) {
                    cancelledBookingApi(getText);

                } else {
                    Toast.makeText(BookingsDetialsActivity.this, "Cancellation Reason Required", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.show();
    }


}