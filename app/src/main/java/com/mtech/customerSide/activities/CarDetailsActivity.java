package com.mtech.customerSide.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.customerSide.R;
import com.mtech.customerSide.SMS.SMSActivity;
import com.mtech.customerSide.adapters.ImageSliderAdapter;
import com.mtech.customerSide.adapters.SpecificationAdapter;
import com.mtech.customerSide.model.SpecificationsModel;
import com.mtech.customerSide.model.home.VehicleImageModel;
import com.mtech.customerSide.model.home.VehicleSpecifciationModel;
import com.mtech.customerSide.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

public class CarDetailsActivity extends AppCompatActivity {
    TextView tv_description, tv_name, tv_price, tvColor;
    public List<VehicleImageModel> vehicleImageModels;
    public List<VehicleSpecifciationModel> vehicleSpecifciationModels;
    ViewPager2 viewPager2;
    Button bbooknow;
    private RecyclerView category_recyclerview;
    private SpecificationAdapter adpter;
    private LinearLayoutManager manager;
    ArrayList<SpecificationsModel> carTypesModels;
    ImageView img_backarrow;
    private Handler ImageSliderHandler = new Handler();
    KProgressHUD progressHUD;
    String carDescription = "", carRate = "", color = "", currency = "",vehicle_Name="";
    TextView tvMessages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_details);
        carDescription = Utilities.getString(CarDetailsActivity.this, "carDescription");
        carRate = Utilities.getString(CarDetailsActivity.this, "carRateperDay");
        color = Utilities.getString(CarDetailsActivity.this, "vehicle_color");
        currency = Utilities.getString(CarDetailsActivity.this, "currency");
        vehicle_Name = Utilities.getString(CarDetailsActivity.this, "vehicle_Name");
        int position = getIntent().getIntExtra("position", -1);
        if (position != -1) {
            vehicleImageModels = Utilities.getVehicleImageModel(this);
            vehicleSpecifciationModels = Utilities.getVehicleSpecificationModels(this);

//            CommentsModel stylistServicesModel = commentsModelList.get(position);
//            if (stylistServicesModel != null) {
//                comentedBy = stylistServicesModel.getCommented_by();
//            }
        }
        init();
        Clicks();
        setData(category_recyclerview, vehicleSpecifciationModels);
        setSliderimage();
    }

    private void Clicks() {
        img_backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tvMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToChat();
            }
        });
    }

    private void init() {
        tv_description = findViewById(R.id.tv_description);
        tv_name = findViewById(R.id.tv_name);
        bbooknow = findViewById(R.id.bbooknow);
        tvMessages = findViewById(R.id.tvMessages);
        img_backarrow = findViewById(R.id.img_backarrow);
        tvColor = findViewById(R.id.tvColor);
        viewPager2 = findViewById(R.id.imageSlider_Viewpager);
        tv_price = findViewById(R.id.tv_price);

        carTypesModels = new ArrayList<>();
        category_recyclerview = findViewById(R.id.category_recyclerview);
        tv_description.setText(carDescription);
        tv_price.setText(currency + carRate + "/Day");
        tvColor.setText(vehicle_Name+"("+color+")");
        bbooknow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), BookCarActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    public void moveback(View view) {
        finish();
    }


    public void goToChat() {
        startActivity(new Intent(CarDetailsActivity.this, SMSActivity.class));
//        Toast.makeText(this, "Under Developing", Toast.LENGTH_SHORT).show();

    }

    private void setData(RecyclerView rvAvailableDate, List dataList) {

        adpter = new SpecificationAdapter(this, dataList);
        LinearLayoutManager manager = new LinearLayoutManager(CarDetailsActivity.this, RecyclerView.HORIZONTAL, false);
        rvAvailableDate.setHasFixedSize(true);
        rvAvailableDate.setLayoutManager(manager);
        rvAvailableDate.setAdapter(adpter);
    }

    private void setSliderimage() {
        viewPager2.setAdapter(new ImageSliderAdapter(CarDetailsActivity.this, vehicleImageModels, viewPager2));
        viewPager2.setClipToPadding(false);
        viewPager2.setClipChildren(false);
        viewPager2.setOffscreenPageLimit(2);
        viewPager2.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(40));
        compositePageTransformer.addTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {


                float a = 1 - Math.abs(position);
                page.setScaleY(0.85f + a * 0.15f);
            }
        });

        viewPager2.setPageTransformer(compositePageTransformer);
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                ImageSliderHandler.removeCallbacks(imagesliderRunable);
                ImageSliderHandler.postDelayed(imagesliderRunable, 3000);
            }


        });
    }

    private Runnable imagesliderRunable = new Runnable() {
        @Override
        public void run() {

            viewPager2.setCurrentItem(viewPager2.getCurrentItem() + 1);
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        ImageSliderHandler.removeCallbacks(imagesliderRunable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ImageSliderHandler.removeCallbacks(imagesliderRunable);
    }

}