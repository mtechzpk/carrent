package com.mtech.customerSide.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;

import com.mtech.customerSide.R;
import com.mtech.customerSide.utils.Utilities;

public class SplashScreenActivity extends AppCompatActivity {
    Button btn_login, btn_signUp;
    LinearLayout llBottom;
    Animation uptodown, downtoup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen_activcity);
        String loginStatus = Utilities.getString(SplashScreenActivity.this, "login_status");
        init();
        clicks();
//        llBottom = findViewById(R.id.llBottom);
//        uptodown = AnimationUtils.loadAnimation(this, R.anim.uptodown);
//        downtoup = AnimationUtils.loadAnimation(this, R.anim.downtoup);
//
////        rllogo.setAnimation(downtoup);
//        llBottom.setAnimation(uptodown);
//        RotateAnimation rotate = new RotateAnimation(0, 720, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//        rotate.setDuration(3000);
//        rotate.setInterpolator(new LinearInterpolator());
//            rllogo.startAnimation(rotate);
        Thread myThread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(1500);
                    if (loginStatus.equals("customer")) {
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(intent);
                        finish();

//                        llBottom.setVisibility(View.GONE);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        myThread.start();


    }

    private void clicks() {
        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });
    }

    private void init() {
        btn_login = findViewById(R.id.btn_login);
        btn_signUp = findViewById(R.id.btn_signUp);
    }
}