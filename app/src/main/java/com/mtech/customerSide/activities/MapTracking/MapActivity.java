package com.mtech.customerSide.activities.MapTracking;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.customerSide.R;
import com.mtech.customerSide.model.CommonResponseModel;
import com.mtech.customerSide.networks.GetDateService;
import com.mtech.customerSide.networks.RetrofitClientInstance;
import com.mtech.customerSide.utils.Utilities;
import com.varunjohn1990.iosdialogs4android.IOSDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    KProgressHUD hud;
    private GoogleMap mMap;
    ImageView img_backarrow;
    LatLng previousLatLng;
    LatLng currentlatlng, destLatLng;

    private List<LatLng> polylinePoints = new ArrayList<>();
    private Marker mCurrLocationMarker;

    Button bEndBooking;
    private FusedLocationProviderClient mLocationProviderClient;
    private LocationCallback locationUpdatesCallback;

    Polyline polyline1;
    String from = "";
    String vehicle_name = "";

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    Context context;
    LatLng dest;
    String dest_lat = "";
    String dest_lng = "";
    int booking_request_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        img_backarrow = findViewById(R.id.img_backarrow);
        img_backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Bundle bundle = getIntent().getExtras();
//        from = bundle.getString("from");
//        dest_lat = Utilities.getString(this,"drop_lat");
//        dest_lng = Utilities.getString(this,"drop_lng");

        booking_request_id = Utilities.getInt(this, "booking_request_id");
        vehicle_name = Utilities.getString(this, "vehicle_name");
        dest_lat = "33.55044";
        dest_lng = "73.09559";
        from = "transport";


        bEndBooking = findViewById(R.id.bEndBooking);
        mLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        context = MapActivity.this;
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationUpdatesCallback = new LocationCallback();

        bEndBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LocationNotification.cancel(MapActivity.this);
                mLocationProviderClient.removeLocationUpdates(locationUpdatesCallback);
                stopService(new Intent(context, LocationTrackerService.class));


                if (from.equals("transport")) {

//                    completeTransportApi(bundle.getString("ride_id"));

                } else {

//                    completeShipmentApi(bundle.getString("ride_id"));

                }
                endBookingApi();

//                Intent intent = new Intent(MapActivity.this, CompleteBookingActivity.class);
//                intent.putExtras(bundle);
//                startActivity(intent);

            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        mMap = googleMap;

        polyline1 = mMap.addPolyline(new PolylineOptions().addAll(polylinePoints));
        fetchLocationUpdates();

    }


    private void fetchLocationUpdates() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference().child("location").child("device1");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Log.i("tag", "New location updated:" + dataSnapshot.getKey());
                updateMap(dataSnapshot);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateMap(DataSnapshot dataSnapshot) {
        double latitude = 0, longitude = 0;

        Iterable<DataSnapshot> data = dataSnapshot.getChildren();
        for (DataSnapshot d : data) {
            if (d.getKey().equals("latitude")) {
                latitude = (Double) d.getValue();
            } else if (d.getKey().equals("longitude")) {
                longitude = (Double) d.getValue();
            }
        }

        currentlatlng = new LatLng(latitude, longitude);
        destLatLng = new LatLng(Double.parseDouble(dest_lat), Double.parseDouble(dest_lng));
        dest = new LatLng(Double.parseDouble(dest_lat), Double.parseDouble(dest_lng));

        if (previousLatLng == null || previousLatLng != destLatLng) {
            // add marker line
            if (mMap != null) {
//
//                previousLatLng = currentLatLng;
//                polylinePoints.add(currentLatLng);
//                polyline1.setPoints(polylinePoints);

                Log.w("tag", "Key:" + currentlatlng);
                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.setPosition(destLatLng);
                } else {
                    mCurrLocationMarker = mMap.addMarker(new MarkerOptions()
                            .position(destLatLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.car))
                            .title(vehicle_name));

                }

                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dest, 14));
            }

            MarkerOptions markerOptions = new MarkerOptions().position(currentlatlng);
            mMap.addMarker(markerOptions);


        }
    }

    private void endBookingApi() {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();
        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<CommonResponseModel> call = service.end_booking(String.valueOf(booking_request_id));
        call.enqueue(new Callback<CommonResponseModel>() {
            @Override
            public void onResponse(Call<CommonResponseModel> call, Response<CommonResponseModel> response) {

                assert response.body() != null;
                int status = response.body().getStatus();
                //String profileImage = response.body().getData().getProfile_image();
                if (status == 200) {
                    hud.dismiss();
                    new IOSDialog.Builder(MapActivity.this)
                            .message(response.body().getMessage()) // String or String Resource ID
                            .build()
                            .show();

                } else {
                    hud.dismiss();
                    Toast.makeText(MapActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<CommonResponseModel> call, Throwable t) {

                hud.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}