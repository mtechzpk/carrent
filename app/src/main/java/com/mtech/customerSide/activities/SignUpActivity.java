package com.mtech.customerSide.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.customerSide.R;
import com.mtech.customerSide.model.CommonResponseModel;
import com.mtech.customerSide.networks.GetDateService;
import com.mtech.customerSide.networks.RetrofitClientInstance;
import com.mtech.customerSide.utils.Utilities;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {


    private ImageView back_ic;
    private EditText edname, ed_Phone, ed_email, ed_phonenumber;
    private ShowHidePasswordEditText ed_password, ed_confirm_password;
    private Button btn_signup;
    TextView tv_login;
    ProgressDialog progressDialog;
    KProgressHUD hud;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        back_ic = findViewById(R.id.back_ic);
        tv_login = findViewById(R.id.tv_login);
        edname = findViewById(R.id.edname);
        ed_Phone = findViewById(R.id.ed_Phone);
        ed_email = findViewById(R.id.etEmail);
        ed_password = findViewById(R.id.ed_password);
        ed_confirm_password = findViewById(R.id.ed_confirm_password);
        ed_phonenumber = findViewById(R.id.ed_Phone);
        btn_signup = findViewById(R.id.btn_signup);

        progressDialog = new ProgressDialog(this, AlertDialog.THEME_HOLO_DARK);
        progressDialog.setMessage("Creating User...");

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
        ed_email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return false;
            }
        });
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String getName = edname.getText().toString();
                String email = ed_email.getText().toString();
                String phone = ed_phonenumber.getText().toString();
                String password = ed_password.getText().toString();
                String cpassword = ed_confirm_password.getText().toString();

                if (getName.isEmpty() || email.isEmpty() || phone.isEmpty() || password.isEmpty() || cpassword.isEmpty() || !password.matches(cpassword)) {
                    Toast.makeText(SignUpActivity.this, "Please Enter Missing Fields or Correct Password.", Toast.LENGTH_SHORT).show();
                } else if (!email.matches(emailPattern)) {

                    Toast.makeText(SignUpActivity.this, "Invalid Email Address", Toast.LENGTH_SHORT).show();
                } else {
//                    Intent intent = new Intent(SignUpActivity.this, OTPActivity.class);
//                    Utilities.saveString(SignUpActivity.this, "userName", getName);
//                    Utilities.saveString(SignUpActivity.this, "userPhone", phone);
//                    Utilities.saveString(SignUpActivity.this, "userPass", password);
//                    Utilities.saveString(SignUpActivity.this, "userConfirmPass", cpassword);
//                    Utilities.saveString(SignUpActivity.this, "userEmail", email);
//                    startActivity(intent);
                    check_credentialsApi(email, phone, getName, password, cpassword);
                }

            }

        });

        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


    public void signIn() {

        startActivity(new Intent(SignUpActivity.this, LoginActivity.class));

    }

    private void check_credentialsApi(String email, String phone, String getName, String password, String cpassword) {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();
        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<CommonResponseModel> call = service.check_credentials(email, phone);
        call.enqueue(new Callback<CommonResponseModel>() {
            @Override
            public void onResponse(Call<CommonResponseModel> call, Response<CommonResponseModel> response) {

                assert response.body() != null;
                int status = response.body().getStatus();
                //String profileImage = response.body().getData().getProfile_image();
                if (status == 200) {
                    hud.dismiss();
                    Intent intent = new Intent(SignUpActivity.this, OTPActivity.class);
                    Utilities.saveString(SignUpActivity.this, "userName", getName);
                    Utilities.saveString(SignUpActivity.this, "userPhone", phone);
                    Utilities.saveString(SignUpActivity.this, "userPass", password);
                    Utilities.saveString(SignUpActivity.this, "userConfirmPass", cpassword);
                    Utilities.saveString(SignUpActivity.this, "userEmail", email);
                    startActivity(intent);

                } else {
                    hud.dismiss();
                    Toast.makeText(SignUpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<CommonResponseModel> call, Throwable t) {

                hud.dismiss();
                t.printStackTrace();
            }
        });
    }
}