package com.mtech.customerSide.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.customerSide.R;
import com.mtech.customerSide.model.CommonResponseModel;
import com.mtech.customerSide.networks.GetDateService;
import com.mtech.customerSide.networks.RetrofitClientInstance;
import com.mtech.customerSide.utils.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassActivity extends AppCompatActivity {
    EditText etEmail;
    Button bContinue;
    KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);
        bContinue = findViewById(R.id.bContinue);
        etEmail = findViewById(R.id.etEmail);
        bContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getEmail = etEmail.getText().toString();
                if (!TextUtils.isEmpty(getEmail)) {
                    forgotPass(getEmail);

                }
                else {
                    Toast.makeText(ForgotPassActivity.this, "Enter Email to Continue", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void forgotPass(String email) {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();
        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<CommonResponseModel> call = service.forgot_password(email);
        call.enqueue(new Callback<CommonResponseModel>() {
            @Override
            public void onResponse(Call<CommonResponseModel> call, Response<CommonResponseModel> response) {

                assert response.body() != null;
                int status = response.body().getStatus();
                //String profileImage = response.body().getData().getProfile_image();
                if (status == 200) {
                    hud.dismiss();
                    startActivity(new Intent(ForgotPassActivity.this,VerifyCodeActivity.class));

                    Toast.makeText(ForgotPassActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Utilities.saveString(ForgotPassActivity.this,"userEmail",email);
                } else {
                    hud.dismiss();
                    Toast.makeText(ForgotPassActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<CommonResponseModel> call, Throwable t) {

                hud.dismiss();
                Toast.makeText(ForgotPassActivity.this,t.getMessage(), Toast.LENGTH_SHORT).show();

                t.printStackTrace();
            }
        });
    }

}