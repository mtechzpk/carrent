package com.mtech.customerSide.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.goodiebag.pinview.Pinview;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.customerSide.R;
import com.mtech.customerSide.networks.GetDateService;
import com.mtech.customerSide.networks.UrlController;
import com.mtech.customerSide.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPActivity extends AppCompatActivity {
    Pinview editTextCode;
    //These are the objects needed
    //It is the verification id that will be sent to the user
    private String mVerificationId;
    KProgressHUD hud;
    //The edittext to input the code
    TextView tvPhoneNumber, ResendCode;
    //firebase auth object
    KProgressHUD dialog0;
    TextView counter;
    private FirebaseAuth mAuth;
    ImageView back_ic;
    String phonenumber = "", userName = "", userPass = "", userConfirmPass = "", userEmail = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p);
        counter = findViewById(R.id.counter);
        ResendCode = findViewById(R.id.ResendCode);
        editTextCode = findViewById(R.id.otp_view);
        back_ic = findViewById(R.id.back_ic);
        phonenumber = Utilities.getString(OTPActivity.this, "userPhone");
        userName = Utilities.getString(OTPActivity.this, "userName");
        userPass = Utilities.getString(OTPActivity.this, "userPass");
        userConfirmPass = Utilities.getString(OTPActivity.this, "userConfirmPass");
        userEmail = Utilities.getString(OTPActivity.this, "userEmail");


        tvPhoneNumber = findViewById(R.id.tvPhoneNumber);
        tvPhoneNumber.setText("Please type  the verification code sent to your Phone Number  " + "" + phonenumber);
        //initializing objects

        mAuth = FirebaseAuth.getInstance();
        sendVerificationCode(phonenumber);
        downTimer();
        //getting mobile number from the previous activity
        //and sending the verification code to the number
//        Intent intent = getIntent();
//        String mobile = intent.getStringExtra("mobile");
//        sendVerificationCode(mobile);
        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth = FirebaseAuth.getInstance();
                downTimer();
                mAuth = FirebaseAuth.getInstance();
                ResendCode.setVisibility(View.GONE);
                Intent intent = getIntent();
                sendVerificationCode(phonenumber);

//                String mobile = intent.getStringExtra("mobile");
//                sendVerificationCode(mobile);
            }
        });
        //if the automatic sms detection did not work, user can also enter the code manually
        //so adding a click listener to the button
        findViewById(R.id.btn_continue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = editTextCode.getValue();
                if (code.isEmpty() || code.length() < 6) {
                    editTextCode.requestFocus();
                    return;
                }

                //verifying the code entered manually
                verifyVerificationCode(code);
            }
        });

    }

    //the method is sending verification code
    //the country id is concatenated
    //you can take the country id as user input as well
    private void sendVerificationCode(String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "" + mobile,
//                mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }

    private void downTimer() {
        new CountDownTimer(60 * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long second = (millisUntilFinished / 1000) % 60;
                long minutes = (millisUntilFinished / (1000 * 60)) % 60;
                counter.setText(minutes + ":" + second + " sec");
            }

            @Override
            public void onFinish() {
//                counter.setText("Finish");
                ResendCode.setVisibility(View.VISIBLE);
            }
        }.start();
    }
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                editTextCode.setValue(code);
                //verifying the code
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(OTPActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            mVerificationId = s;
        }
    };


    private void verifyVerificationCode(String code) {
        //creating the credential
//        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
        if(code.equals("") && phonenumber.equals(""))
            Toast.makeText(OTPActivity.this,"Nothing to validate", Toast.LENGTH_SHORT).show();
        else {
            try {
                PhoneAuthCredential credential1 = PhoneAuthProvider.getCredential(mVerificationId, code);
                signInWithPhoneAuthCredential(credential1);
            }
            catch (Exception e) {
                Log.i("exception",e.toString());
                Toast.makeText(this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                Toast.makeText(OTPActivity.this,"Invalid credentials",Toast.LENGTH_LONG).show();
            }

        }
        //signing the user

    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(OTPActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //verification successful we will start the profile activity
                            Toast.makeText(OTPActivity.this, "success", Toast.LENGTH_SHORT).show();
                            signupApi(userName, userEmail, phonenumber, userPass, userConfirmPass);
                        } else {
                            //verification unsuccessful.. display an error message

                            String message = "Somthing is wrong, we will fix it soon...";

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            }

                            Snackbar snackbar = Snackbar.make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
                            snackbar.setAction("Dismiss", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });
                            snackbar.show();
                        }
                    }
                });
    }

    private void signupApi(final String name, String email, String phone, String pass, String cPass) {


        dialog0 = KProgressHUD.create(OTPActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        GetDateService restService = UrlController.createService(GetDateService.class);

        Call<ResponseBody> myCall = restService.signupApi(name, email, phone, pass, cPass);
        myCall.enqueue(new Callback<ResponseBody>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> responseObj) {
                try {

                    if (responseObj.isSuccessful()) {

                        dialog0.dismiss();

                        JSONObject response = new JSONObject(responseObj.body().string());
                        int status = response.getInt("status");
                        String message = response.getString("message");

                        if (status == 200) {
                            dialog0.dismiss();
                            Toast.makeText(OTPActivity.this, message, Toast.LENGTH_SHORT).show();

                            JSONObject jsonObject1 = response.getJSONObject("data");
                            int id = jsonObject1.getInt("id");
                            String name = jsonObject1.getString("name");
                            String email = jsonObject1.getString("email");
                            String phone = jsonObject1.getString("phone");
                            String profileImage = jsonObject1.getString("profile_image");
//                    String longitude = response.body().getData().getLongitude();

                            Intent intent = new Intent(OTPActivity.this, HomeActivity.class);
                            Utilities.saveString(OTPActivity.this, "login_status", "customer");

                            Utilities.saveString(OTPActivity.this, Utilities.USER_FNAME, name);
                            Utilities.saveString(OTPActivity.this, Utilities.USER_ID, String.valueOf(id));
                            Utilities.saveString(getApplicationContext(), "userEmail", userEmail);
                            Utilities.saveString(getApplicationContext(), "userPhone", phone);
                            Utilities.saveString(getApplicationContext(), "userName", name);
                            Utilities.saveString(getApplicationContext(), "support_phone", jsonObject1.getString("support_phone"));
                            Utilities.saveString(getApplicationContext(), "support_email", jsonObject1.getString("support_email"));
                            Utilities.saveString(getApplicationContext(), "security_deposite", jsonObject1.getString("security_deposite"));
                            Utilities.saveString(getApplicationContext(), "security_deposite_currency", jsonObject1.getString("security_deposite_currency"));


                            Utilities.saveString(getApplicationContext(), Utilities.USER_PROFILE_IMAGE, profileImage);
                            startActivity(intent);
                            finish();
                        } else if (status == 400) {

                            Toast.makeText(OTPActivity.this, message, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        dialog0.dismiss();
                        Toast.makeText(OTPActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {

                    dialog0.dismiss();

                    e.printStackTrace();
//                    Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();

                } catch (IOException e) {

                    dialog0.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                dialog0.dismiss();

                Log.d("info Login error", String.valueOf(t));
                Log.d("info Login error", String.valueOf(t.getMessage() + t.getCause() + t.fillInStackTrace()));
            }
        });


    }
}