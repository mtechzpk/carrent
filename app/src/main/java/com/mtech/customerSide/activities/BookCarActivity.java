package com.mtech.customerSide.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.customerSide.GpsLocation.GPSTracker;
import com.mtech.customerSide.R;
import com.mtech.customerSide.adapters.MyInfoWindowAdapter;
import com.mtech.customerSide.utils.Utilities;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class BookCarActivity extends AppCompatActivity implements OnMapReadyCallback {
    ImageView img_backarrow, iv_profile;
    TextView ed_select_pick_date, ed_select_pick_time, ed_select_return_date, ed_select_return_time;
    MaterialSpinner spinner_payment_method;
    final Calendar myCalendar = Calendar.getInstance();
    String _paymentMethode = "";
    String pickupLatitude = "", pickupLongitude = "";
    String returnLatitude = "", returnLongitude = "";
    Uri imageUri;
    Bitmap selected_img_bitmap;
    String input = "";
    KProgressHUD hud;
    String getPickTime = "", getReturnTime = "", getPickDate = "", getReturnDate = "";
    Button btn_book;
    String imageEncoded;
    String pickuDateTime = "", returnkuDateTime = "";
    //Map declaration
    EditText etName, etPhone, etAddress;
    private GoogleMap myGoogleMap;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Location currentlocation;
    private AutocompleteSupportFragment autocompleteFragment, autocompleteReturn;
    private static final int REQUEST_CODE = 103;
    private LatLng selectedLatLng, selectedReturnLatLng;
    private LinearLayout buttonCurrentLocation;
    String address = "";
    Calendar date1;
    String latitude, longitude, vehicle_id = "", getPickupAddress = "", getReturnAddresss = "";
    TextView buttonSaveLocation;
    private static final int MAX_YEAR = 0;
    String userID = "", vehicle_NamewithColor = "", userPhone = "", userName = "", carDescription = "", vehicle_color = "", vehicle_Name = "", carRate = "", currency = "";
    TextView selectPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_car);
        userID = Utilities.getString(BookCarActivity.this, Utilities.USER_ID);
        vehicle_id = Utilities.getString(BookCarActivity.this, "vehicle_id");
        userPhone = Utilities.getString(BookCarActivity.this, "userPhone");
        userName = Utilities.getString(BookCarActivity.this, "userName");
        carDescription = Utilities.getString(BookCarActivity.this, "carDescription");
        vehicle_color = Utilities.getString(BookCarActivity.this, "vehicle_color");
        vehicle_Name = Utilities.getString(BookCarActivity.this, "vehicle_Name");
        carRate = Utilities.getString(BookCarActivity.this, "carRateWithCurrency");
        currency = Utilities.getString(BookCarActivity.this, "currency");

        vehicle_NamewithColor = vehicle_Name + " (" + vehicle_color + ") ";
        init();
        Clicks();
        etName.setText(userName);
        etPhone.setText(userPhone);
        final Calendar currentDate = Calendar.getInstance();
        date1 = Calendar.getInstance();


        GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.getIsGPSTrackingEnabled()) {
            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
            fetchLastLocation();
        } else {
            gpsTracker.showSettingsAlert();
        }
        buttonSaveLocation = findViewById(R.id.btn_save_location);
        buttonCurrentLocation = findViewById(R.id.ll_current_location);

        //For AutoComplete Search Places
        Places.initialize(getApplicationContext(), Utilities.MAP_KEY, Locale.getDefault());
        PlacesClient placesClient = Places.createClient(getApplicationContext());
        // Initialize the AutocompleteSupportFragment.
        autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        autocompleteReturn = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocompleteReturn);

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG));


        autocompleteReturn.setPlaceFields(Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG));


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //hideKeyboard (btn_book,getApplicationContext ());

        //date picker
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


        ed_select_pick_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener dateSetListener = new
                        DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int
                                    monthOfYear, int dayOfMonth) {
                                date1.set(year, monthOfYear, dayOfMonth);
                                getPickDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                                ed_select_pick_date.setText(getPickDate);


                                //use this date as per your requirement
                            }
                        };
                DatePickerDialog datePickerDialog = new
                        DatePickerDialog(BookCarActivity.this, dateSetListener,
                        currentDate.get(Calendar.YEAR),
                        currentDate.get(Calendar.MONTH),
                        currentDate.get(Calendar.DAY_OF_MONTH));
                // Limiting access to past dates in the step below:
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });


        //date return
        DatePickerDialog.OnDateSetListener datereturn = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabelreturn();
            }

        };


        ed_select_return_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener dateSetListener = new
                        DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int
                                    monthOfYear, int dayOfMonth) {
                                date1.set(year, monthOfYear, dayOfMonth);
                                getReturnDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                                ed_select_return_date.setText(getReturnDate);


                                //use this date as per your requirement
                            }
                        };
                DatePickerDialog datePickerDialog = new
                        DatePickerDialog(BookCarActivity.this, dateSetListener,
                        currentDate.get(Calendar.YEAR),
                        currentDate.get(Calendar.MONTH),
                        currentDate.get(Calendar.DAY_OF_MONTH));
                // Limiting access to past dates in the step below:
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });


        //Pick time picker
        ed_select_pick_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                final Calendar myCalender = Calendar.getInstance();
                int hour = myCalender.get(Calendar.HOUR_OF_DAY);
                int minute = myCalender.get(Calendar.MINUTE);


                TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (view.isShown()) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            myCalender.set(Calendar.MINUTE, minute);
                            ed_select_pick_time.setText(hourOfDay + ":" + minute);
                            getPickTime = hourOfDay + ":" + minute;

                        }
                    }
                };
                TimePickerDialog timePickerDialog = new TimePickerDialog(BookCarActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, true);
                timePickerDialog.setTitle("Choose Pick Time:");
                timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                timePickerDialog.show();
            }
        });


//        droptime picker  ed_select_return_time

        ed_select_return_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar myCalender = Calendar.getInstance();
                int hour = myCalender.get(Calendar.HOUR_OF_DAY);
                int minute = myCalender.get(Calendar.MINUTE);


                TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (view.isShown()) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            myCalender.set(Calendar.MINUTE, minute);
                            ed_select_return_time.setText(hourOfDay + ":" + minute);
                            getReturnTime = hourOfDay + ":" + minute;

                        }
                    }
                };
                TimePickerDialog timePickerDialog = new TimePickerDialog(BookCarActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, true);
                timePickerDialog.setTitle("Choose Return Time:");
                timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                timePickerDialog.show();
            }
        });

        Timer updateTimer = new Timer();
        updateTimer.schedule(new TimerTask() {
            public void run() {
                try {

                    SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss aa");
                    Date date1 = format.parse("08:00:12 pm");
                    Date date2 = format.parse("05:30:12 pm");
                    long mills = date1.getTime() - date2.getTime();
                    Log.v("Data1", "" + date1.getTime());
                    Log.v("Data2", "" + date2.getTime());
                    int hours = (int) (mills / (1000 * 60 * 60));
                    int mins = (int) (mills / (1000 * 60)) % 60;

                    String diff = hours + ":" + mins; // updated value every1 second
                    Toast.makeText(BookCarActivity.this, "" + diff, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }, 0, 1000);
        //spinner_payment_method
        spinner_payment_method.setItems("PayPal", "Cash On Delivery");
        spinner_payment_method.setOnItemSelectedListener((MaterialSpinner.OnItemSelectedListener<String>) (view, position, id, item) ->
                _paymentMethode = item
        );


    }

    private void Clicks() {
        img_backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        selectPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doProfilePhoto();
            }
        });
        iv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doProfilePhoto();
            }
        });

        btn_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String customerName = etName.getText().toString();
                String customerAddres = etAddress.getText().toString();
                String customerPhone = etPhone.getText().toString();

                if (imageUri != null && !imageUri.equals(Uri.EMPTY)) {
                    if (!getPickDate.equals("")) {
                        if (!getPickTime.equals("")) {
                            if (!getReturnDate.equals("")) {
                                if (!getReturnTime.equals("")) {
                                    if (!getPickupAddress.equals("")) {
                                        if (!pickupLatitude.equals("")) {
                                            if (!pickupLongitude.equals("")) {
                                                if (!getReturnAddresss.equals("")) {
                                                    if (!_paymentMethode.equals("")) {
                                                        pickuDateTime = getPickDate + " " + getPickTime;
                                                        returnkuDateTime = getReturnDate + " " + getReturnTime;
                                                        getDaysBetweenDates(getPickDate, getReturnDate);
                                                        getTimeDifference(getPickTime, getReturnTime);
                                                        SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
                                                        boolean b = false;
                                                        try {
                                                            if (dfDate.parse(getPickDate).before(dfDate.parse(getReturnDate))) {
                                                                b = true;//If start date is before end date
                                                                Utilities.saveString(BookCarActivity.this, "bookPickDateTime", pickuDateTime);
                                                                Utilities.saveString(BookCarActivity.this, "returnkuDateTime", returnkuDateTime);
                                                                Utilities.saveString(BookCarActivity.this, "getPickupAddress", getPickupAddress);
                                                                Utilities.saveString(BookCarActivity.this, "pickupLatitude", pickupLatitude);
                                                                Utilities.saveString(BookCarActivity.this, "pickupLongitude", pickupLongitude);
                                                                Utilities.saveString(BookCarActivity.this, "getReturnAddresss", getReturnAddresss);
                                                                Utilities.saveString(BookCarActivity.this, "returnLatitude", returnLatitude);
                                                                Utilities.saveString(BookCarActivity.this, "returnLongitude", returnLongitude);
                                                                Utilities.saveString(BookCarActivity.this, "_paymentMethode", _paymentMethode);

                                                                Utilities.saveString(BookCarActivity.this, "customerName", customerName);
                                                                Utilities.saveString(BookCarActivity.this, "customerPhone", customerPhone);
                                                                Utilities.saveString(BookCarActivity.this, "customerAddres", customerAddres);
                                                                Utilities.saveString(BookCarActivity.this, "drivingLiscenseImage", input);

                                                                startActivity(new Intent(BookCarActivity.this, BookingConfirmedActivity.class));


                                                            } else if (dfDate.parse(getPickDate).equals(dfDate.parse(getReturnDate))) {
                                                                b = true;//If two dates are equal
                                                                Toast.makeText(BookCarActivity.this, "Invalid Date , Both Dates Are Equal  ,Pickup Date should be less than Return Date", Toast.LENGTH_SHORT).show();

                                                            } else {
                                                                b = false; //If start date is after the end date
                                                                Toast.makeText(BookCarActivity.this, "Invalid Date ,Pickup Date should be less than Return Date", Toast.LENGTH_SHORT).show();

                                                            }
                                                        } catch (ParseException e) {
                                                            // TODO Auto-generated catch block
                                                            e.printStackTrace();
                                                        }


                                                    } else {
                                                        Toast.makeText(getApplicationContext(), "Payment Method missing", Toast.LENGTH_SHORT).show();
                                                    }

                                                } else {
                                                    Toast.makeText(getApplicationContext(), "Return Location missing", Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                Toast.makeText(getApplicationContext(), "Pickup longitude missing", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(getApplicationContext(), "pickup latitude missing", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Pickup Location not found", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), "Return Time Missing", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Pick Time missing", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Pick Time missing", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Pick Date missing", Toast.LENGTH_SHORT).show();


                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Driving License Image missing", Toast.LENGTH_SHORT).show();


                }
            }
        });

    }

    private void init() {
        img_backarrow = findViewById(R.id.img_backarrow);
        ed_select_pick_date = findViewById(R.id.ed_select_pick_date);
        ed_select_pick_time = findViewById(R.id.ed_select_pick_time);
        etName = findViewById(R.id.etName);
        iv_profile = findViewById(R.id.iv_profile);
        etAddress = findViewById(R.id.etAddress);
        selectPhoto = findViewById(R.id.selectPhoto);
        etPhone = findViewById(R.id.etPhone);
        ed_select_return_time = findViewById(R.id.ed_select_return_time);

        ed_select_return_date = findViewById(R.id.ed_select_return_date);

        spinner_payment_method = findViewById(R.id.spinner_payment_method);

        btn_book = findViewById(R.id.btn_book);

    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        ed_select_pick_date.setText(sdf.format(myCalendar.getTime()));
    }


    //return
    private void updateLabelreturn() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        ed_select_return_date.setText(sdf.format(myCalendar.getTime()));
    }


    private void fetchLastLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            return;
        }

        Task<Location> task = fusedLocationProviderClient.getLastLocation()
                .addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {

                        if (task.isSuccessful()) {

                            currentlocation = task.getResult();
                            selectedLatLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));

                            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map);
                            mapFragment.getMapAsync(BookCarActivity.this::onMapReady);

                        }
                    }
                });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        myGoogleMap = googleMap;

        //Setting the Custom Marker Title
        myGoogleMap.setInfoWindowAdapter(new MyInfoWindowAdapter(getApplicationContext()));

        //First show the Selected Location that is stored in the SharedPref, If no location is saved
        //then show the current location
        // showSavedLatLng returns TRUE if the savedLocation is successfully shown on map otherwise FALSE


        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            private @NotNull Place place;

            @Override
            public void onPlaceSelected(@NotNull Place place) {
                // TODO: Get info about the selected place.

                selectedLatLng = place.getLatLng();

                if (selectedLatLng != null) {
                    pickupLatitude = String.valueOf(selectedLatLng.latitude);
                    pickupLongitude = String.valueOf(selectedLatLng.longitude);

                    getPickupAddress = Utilities.getAddress(getApplicationContext(), selectedLatLng);
                    Utilities.saveString(BookCarActivity.this, "getAddress", address);

                }

            }

            @Override
            public void onError(@NotNull Status status) {
                // TODO: Handle the error.
                Log.i("YAM", "An error occurred: " + status);
            }
        });

        autocompleteReturn.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NotNull Place place) {
                // TODO: Get info about the selected place.

                selectedLatLng = place.getLatLng();

                if (selectedLatLng != null) {
                    returnLatitude = String.valueOf(selectedLatLng.latitude);
                    returnLongitude = String.valueOf(selectedLatLng.longitude);
                    getReturnAddresss = Utilities.getAddress(getApplicationContext(), selectedLatLng);

                    Utilities.saveString(BookCarActivity.this, "getAddress", address);
                }

            }


            @Override
            public void onError(@NotNull Status status) {
                // TODO: Handle the error.
                Log.i("YAM", "An error occurred: " + status);
            }
        });
    }

    private void doProfilePhoto() {

        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropMenuCropButtonTitle("Done")
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .start(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                imageUri = result.getUri();


                try {

                    selected_img_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                    iv_profile.setImageBitmap(selected_img_bitmap);

                    Bitmap immagex = selected_img_bitmap;
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] b = baos.toByteArray();
                    imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

                    input = imageEncoded;
                    input = input.replace("\n", "");
                    input = input.trim();
                    input = "data:image/png;base64," + input;
                    Utilities.saveString(BookCarActivity.this, "input_profile_image", input);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }


    public String getDaysBetweenDates(String Created_date_String, String Expire_date_String) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        Date Created_convertedDate = null, Expire_CovertedDate = null, todayWithZeroTime = null;
        try {
            Created_convertedDate = dateFormat.parse(Created_date_String);
            Expire_CovertedDate = dateFormat.parse(Expire_date_String);

            Date today = new Date();

            todayWithZeroTime = dateFormat.parse(dateFormat.format(today));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int c_year = 0, c_month = 0, c_day = 0;

        if (Created_convertedDate.after(todayWithZeroTime)) {
            Calendar c_cal = Calendar.getInstance();
            c_cal.setTime(Created_convertedDate);
            c_year = c_cal.get(Calendar.YEAR);
            c_month = c_cal.get(Calendar.MONTH);
            c_day = c_cal.get(Calendar.DAY_OF_MONTH);

        } else {
            Calendar c_cal = Calendar.getInstance();
            c_cal.setTime(todayWithZeroTime);
            c_year = c_cal.get(Calendar.YEAR);
            c_month = c_cal.get(Calendar.MONTH);
            c_day = c_cal.get(Calendar.DAY_OF_MONTH);
        }


/*Calendar today_cal = Calendar.getInstance();
int today_year = today_cal.get(Calendar.YEAR);
int today = today_cal.get(Calendar.MONTH);
int today_day = today_cal.get(Calendar.DAY_OF_MONTH);
*/

        Calendar e_cal = Calendar.getInstance();
        e_cal.setTime(Expire_CovertedDate);

        int e_year = e_cal.get(Calendar.YEAR);
        int e_month = e_cal.get(Calendar.MONTH);
        int e_day = e_cal.get(Calendar.DAY_OF_MONTH);

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(c_year, c_month, c_day);
        date2.clear();
        date2.set(e_year, e_month, e_day);

        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);
        Utilities.saveString(BookCarActivity.this, "rental_duration", new DecimalFormat("##").format(dayCount));
//        Toast.makeText(this, "" + new DecimalFormat("##").format(dayCount), Toast.LENGTH_SHORT).show();
        return ("" + (int) dayCount + " Day");
    }

    public void getTimeDifference(String pickTime, String returnTime) {

        int hours;
        int min;
        long difference;

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
            Date date1 = simpleDateFormat.parse(pickTime);
            Date date2 = simpleDateFormat.parse(returnTime);

            difference = date2.getTime() - date1.getTime();

            hours = Long.valueOf(TimeUnit.MILLISECONDS.toHours(difference)).intValue();
            min = Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(difference)).intValue();

            hours = (hours < 0 ? -hours : hours);
//            Toast.makeText(this, "" + hours, Toast.LENGTH_SHORT).show();
            Utilities.saveString(BookCarActivity.this, "timeDuration", String.valueOf(hours));
            Log.i("======= Hours", " :: " + hours);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}