package com.mtech.customerSide.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.customerSide.R;
import com.mtech.customerSide.model.CommonResponseModel;
import com.mtech.customerSide.networks.GetDateService;
import com.mtech.customerSide.networks.RetrofitClientInstance;
import com.mtech.customerSide.utils.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPassActivity extends AppCompatActivity {
    EditText etConfirmPass, etPass;
    Button bContinue;
    KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pass);
        etConfirmPass = findViewById(R.id.etConfirmPass);
        etPass = findViewById(R.id.etPass);
        bContinue = findViewById(R.id.bContinue);
        bContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getPass = etPass.getText().toString();
                String getConfirmPass = etConfirmPass.getText().toString();
                String getEmail = Utilities.getString(ResetPassActivity.this, "userEmail");
                if (!TextUtils.isEmpty(getPass)) {
                    if (!TextUtils.isEmpty(getConfirmPass)) {
                    if (getPass.equals(getConfirmPass)) {

                        forgotPass(getEmail, getPass, getConfirmPass);

                    } else {
                        Toast.makeText(ResetPassActivity.this, " Password not matched", Toast.LENGTH_SHORT).show();

                    }} else {
                        Toast.makeText(ResetPassActivity.this, "Enter Confirm Password", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(ResetPassActivity.this, "Enter New Password", Toast.LENGTH_SHORT).show();

                }
            }
        });

    }

    private void forgotPass(String email, String getPass, String getConfirmPass) {

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.black))
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(true)
                .setLabel("Please Wait")
                .setCornerRadius(10)
                .show();
        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<CommonResponseModel> call = service.reset_password(email, getPass, getConfirmPass);
        call.enqueue(new Callback<CommonResponseModel>() {
            @Override
            public void onResponse(Call<CommonResponseModel> call, Response<CommonResponseModel> response) {

                assert response.body() != null;
                int status = response.body().getStatus();
                //String profileImage = response.body().getData().getProfile_image();
                if (status == 200) {
                    hud.dismiss();
                    startActivity(new Intent(ResetPassActivity.this, LoginActivity.class));

                    Toast.makeText(ResetPassActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                } else {
                    hud.dismiss();
                    Toast.makeText(ResetPassActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<CommonResponseModel> call, Throwable t) {
                Toast.makeText(ResetPassActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                hud.dismiss();
                t.printStackTrace();
            }
        });
    }

}