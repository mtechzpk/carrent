package com.mtech.customerSide.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.kaopiz.kprogresshud.BuildConfig;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.customerSide.Chat.InboxActivity;
import com.mtech.customerSide.R;
import com.mtech.customerSide.model.UserResponseModel;
import com.mtech.customerSide.networks.GetDateService;
import com.mtech.customerSide.networks.RetrofitClientInstance;
import com.mtech.customerSide.utils.Utilities;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {
    public NavController navController;
    ImageView ivDrawer;
    private DrawerLayout drawer;
    NavigationView navigationView;
    RelativeLayout rlToolbar;
    KProgressHUD dialog0, hud;
    TextView tvTitle;
    String profileImageRESPONSE = "";
    String imageUrl = "", nameUpdate = "", phoneUpdate = "";
    private int RC_LOCATION_ON_REQUEST = 1235;
    private static final int RC_LOCATION_REQUEST = 1234;
    RelativeLayout main_Container;
    Boolean isChange = false;
    private static final float END_SCALE = 0.7f;
    private LocationRequest locationRequest;
    Bitmap selected_img_bitmap;
    String input = "";
    private ClipboardManager myClipboard;
    private ClipData myClip;
    String utilityName = "", image = "", profileImage = "", input_profile_image = "", userPhone = "", support_phone = "", support_email = "";
    EditText edName;
    Uri imageUri;
    TextView user_name, tvPhoneNumber;
    ImageView img_userProfileImage;
    String imageEncoded;
    ImageView iv_profile, ivLocation;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        drawer = findViewById(R.id.drawer_layout);
        main_Container = findViewById(R.id.main_Container);
//        rlToolbar = findViewById(R.id.rlToolbar);
        navigationView = findViewById(R.id.nav_view);
        tvTitle = findViewById(R.id.tvTitle);
        ivDrawer = findViewById(R.id.ivDrawer);
        ivDrawer = findViewById(R.id.ivDrawer);
        rlToolbar = findViewById(R.id.rlToolbar);
        ivLocation = findViewById(R.id.ivLocation);

        utilityName = Utilities.getString(HomeActivity.this, "userName");
        input_profile_image = Utilities.getString(HomeActivity.this, "input_profile_image");
        support_phone = Utilities.getString(HomeActivity.this, "support_phone");
        support_email = Utilities.getString(HomeActivity.this, "support_email");
        userPhone = Utilities.getString(HomeActivity.this, "userPhone");
        image = Utilities.getString(HomeActivity.this, Utilities.USER_PROFILE_IMAGE);
        profileImage = Utilities.getString(HomeActivity.this, Utilities.USER_PROFILE_IMAGE);
        imageUrl = profileImage;
        nameUpdate = utilityName;
        phoneUpdate = userPhone;

        View hView = navigationView.inflateHeaderView(R.layout.nav_header_main);
        user_name = hView.findViewById(R.id.tv_userName);
        TextView tvDayStatus = hView.findViewById(R.id.tvDayStatus);
        img_userProfileImage = hView.findViewById(R.id.img_userProfileImage);
        tvPhoneNumber = hView.findViewById(R.id.tvPhoneNumber);
        tvPhoneNumber.setText(userPhone);

        user_name.setText(utilityName);
        if (!TextUtils.isEmpty(profileImage)) {
            Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG + profileImage).placeholder(R.drawable.user_ic).into(img_userProfileImage);
//            Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG + profileImage).placeholder(R.drawable.user_ic).into(iv_profile);


        }
        img_userProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                drawer.closeDrawer(GravityCompat.START);

            }
        });
        ivLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("geo:37.7749,-122.4194");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }else{
                    Toast.makeText(HomeActivity.this, "please install map application", Toast.LENGTH_SHORT).show();
                }
                // Search for restaurants nearby

//                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)", 12f, 12f, "Destination");
//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//                intent.setPackage("com.google.android.apps.maps");
//                try
//                {
//                    startActivity(intent);
//                }
//                catch(ActivityNotFoundException ex)
//                {
//                    try
//                    {
//                        Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//                        startActivity(unrestrictedIntent);
//                    }
//                    catch(ActivityNotFoundException innerEx)
//                    {
//                        Toast.makeText(HomeActivity.this, "Please install a maps application", Toast.LENGTH_LONG).show();
//                    }
//                }
            }

        });
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay < 12) {
            tvDayStatus.setText("Good Morning");
        } else if (timeOfDay < 16) {
            tvDayStatus.setText("Good Noon");

        } else if (timeOfDay < 21) {
            tvDayStatus.setText("Good Evening");

        } else {
            tvDayStatus.setText("Good Night");

        }
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawer,
                R.string.open,
                R.string.closed
        ) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                // Scale the View based on current slide offset
                final float diffScaledOffset = slideOffset * (1 - END_SCALE);
                final float offsetScale = 1 - diffScaledOffset;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    main_Container.setScaleX(offsetScale);
                    main_Container.setScaleY(offsetScale);
                }


                // Translate the View, accounting for the scaled width
                final float xOffset = drawerView.getWidth() * slideOffset;
                final float yOffset = drawerView.getHeight() * slideOffset;

                final float xOffsetDiff = main_Container.getWidth() * diffScaledOffset / 2;

                final float xTranslation = xOffset - xOffsetDiff;

                final float yOffsetDiff = main_Container.getHeight() * diffScaledOffset / 15;

                final float yTranslation = yOffset - yOffsetDiff;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                    main_Container.setTranslationX(xTranslation);

                }
            }
        };

        drawer.addDrawerListener(mDrawerToggle);

        drawer.setScrimColor(getResources().getColor(android.R.color.transparent));

        /*Remove navigation drawer shadow/fadding*/
        drawer.setDrawerElevation(0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawer.setElevation(0f);
            main_Container.setElevation(10.0f);
        }
        checkLocationSettingsRequest();
        setUpLocationRequest();
        initNavigation();
    }

    private void initNavigation() {
        ivDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START, true);
            }
        });
        navController = Navigation.findNavController(this, R.id.user_container);
        NavigationUI.setupWithNavController(navigationView, navController);
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if (destination.getLabel() != null) {
                    tvTitle.setText(destination.getLabel());
                    if (destination.getLabel().equals("fragment_profile") || destination.getLabel().equals("fragment_notification") || destination.getLabel().equals("fragment_message") || destination.getLabel().equals("fragment_post_an_ad")) {
                        rlToolbar.setVisibility(View.GONE);

                    } else {
                        rlToolbar.setVisibility(View.VISIBLE);
//                        tvTitle.setText(destination.getLabel());
                    }
                }

            }
        });
        navigationView.getMenu().findItem(R.id.logout).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                item.setVisible(true);
                showCustomDialog();
                drawer.closeDrawer(GravityCompat.START, false);
                return true;
            }
        });
        navigationView.getMenu().findItem(R.id.help).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                drawer.closeDrawer(GravityCompat.START, false);
                showdialogueForHelp();
                return true;
            }
        });
        navigationView.getMenu().findItem(R.id.updateProfile).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                showUpdatedialogue(profileImage);
                drawer.closeDrawer(GravityCompat.START, false);
                return true;
            }
        });

        navigationView.getMenu().findItem(R.id.shareAp).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                drawer.closeDrawer(GravityCompat.START, false);
                return true;
            }
        });

        navigationView.getMenu().findItem(R.id.booking).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                drawer.closeDrawer(GravityCompat.START, false);
                startActivity(new Intent(HomeActivity.this, BookingsActivity.class));
                return true;
            }
        });

        navigationView.getMenu().findItem(R.id.messageCenter).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                drawer.closeDrawer(GravityCompat.START, false);
                startActivity(new Intent(HomeActivity.this, InboxActivity.class));
//                Toast.makeText(HomeActivity.this, "Under Developing", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }

    private void showCustomDialog() {
        final PrettyDialog pDialog = new PrettyDialog(HomeActivity.this);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Logout?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.colorPrimary,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {

                                Utilities.clearSharedPref(HomeActivity.this);
                                startActivity(new Intent(HomeActivity.this, SplashScreenActivity.class));
                                finish();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_red,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, RC_LOCATION_REQUEST);
        } else {
            //startService(new Intent(this, LocationTrackerService.class));
        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == RC_LOCATION_ON_REQUEST && resultCode == Activity.RESULT_OK) {
//            Log.e("tag", "Resolution done");
//            startLocationUpdates();
//        }
//    }

    private void setUpLocationRequest() {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void checkLocationSettingsRequest() {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        SettingsClient locationClient = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> locationSettings = locationClient.checkLocationSettings(builder.build());

        locationSettings.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                if (!task.isSuccessful()) {
                    Log.e("tag", "Exception" + task.getException().getMessage());
                    if (task.getException() instanceof ResolvableApiException) {
                        // show permission request to user
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) task.getException();
                            resolvable.startResolutionForResult(HomeActivity.this,
                                    RC_LOCATION_ON_REQUEST);
                        } catch (IntentSender.SendIntentException sendEx) {
                            // Ignore the error.
                        }
                    }
                } else {
                    startLocationUpdates();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RC_LOCATION_REQUEST && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startLocationUpdates();
        }
    }

    private void doProfilePhoto() {

        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropMenuCropButtonTitle("Done")
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .start(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_LOCATION_ON_REQUEST && resultCode == Activity.RESULT_OK) {
            Log.e("tag", "Resolution done");
            startLocationUpdates();
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                imageUri = result.getUri();


                try {

                    selected_img_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                    iv_profile.setImageBitmap(selected_img_bitmap);

                    Bitmap immagex = selected_img_bitmap;
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] b = baos.toByteArray();
                    imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

                    input = imageEncoded;
                    input = input.replace("\n", "");
                    input = input.trim();
                    input = "data:image/png;base64," + input;
                    isChange = true;
                    Utilities.saveString(HomeActivity.this, "input_profile_image", input);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void updateProfileApiNoImage(String getName, String phonee) {
        dialog0 = KProgressHUD.create(HomeActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<UserResponseModel> call = service.update_profile_imageNoImage(Utilities.getString(HomeActivity.this, Utilities.USER_ID), getName, phonee);

        call.enqueue(new Callback<UserResponseModel>() {
            @Override
            public void onResponse(Call<UserResponseModel> call, Response<UserResponseModel> response) {
                dialog0.dismiss();

                assert response.body() != null;
                int status = response.body().getStatus();
                if (status == 200) {
                    Toast.makeText(HomeActivity.this, "Update Profile Successfully", Toast.LENGTH_SHORT).show();
                    dialog0.dismiss();
                    String name = response.body().getData().getName();
                    int userID = response.body().getData().getId();
                    String userEmail = response.body().getData().getEmail();
                    String userPhone = response.body().getData().getPhone();
                    profileImageRESPONSE = response.body().getData().getProfile_image();
                    Utilities.saveString(HomeActivity.this, Utilities.USER_PROFILE_IMAGE, profileImageRESPONSE);
                    Utilities.saveString(HomeActivity.this, Utilities.USER_FNAME, name);
                    Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG + profileImageRESPONSE).into(img_userProfileImage);
                    Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG + profileImageRESPONSE).into(iv_profile);
                    imageUrl = profileImageRESPONSE;
                    nameUpdate = name;
                    phoneUpdate = userPhone;
                    Utilities.saveString(HomeActivity.this, "userPhone", userPhone);
                    tvPhoneNumber.setText(phoneUpdate);
                    user_name.setText(nameUpdate);
                    edName.setText(nameUpdate);


                } else {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<UserResponseModel> call, Throwable t) {
                dialog0.dismiss();
                t.printStackTrace();
                Toast.makeText(HomeActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void updateProfileApiWithImage(String getName, String image, String getPhone) {
        dialog0 = KProgressHUD.create(HomeActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);

        Call<UserResponseModel> call = service.update_profile_image(Utilities.getString(HomeActivity.this, Utilities.USER_ID), image, getName, getPhone);

        call.enqueue(new Callback<UserResponseModel>() {
            @Override
            public void onResponse(Call<UserResponseModel> call, Response<UserResponseModel> response) {
                dialog0.dismiss();

                assert response.body() != null;
                int status = response.body().getStatus();
                if (status == 200) {
                    Toast.makeText(HomeActivity.this, "Update Profile Successfully", Toast.LENGTH_SHORT).show();
                    dialog0.dismiss();
                    String name = response.body().getData().getName();
                    int userID = response.body().getData().getId();
                    String userEmail = response.body().getData().getEmail();
                    String userPhone = response.body().getData().getPhone();
                    String profileImage = response.body().getData().getProfile_image();
                    Utilities.saveString(HomeActivity.this, Utilities.USER_PROFILE_IMAGE, profileImage);
                    Utilities.saveString(HomeActivity.this, Utilities.USER_FNAME, name);
                    Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG + profileImage).into(img_userProfileImage);
                    Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG + profileImage).into(iv_profile);
                    imageUrl = profileImage;
                    nameUpdate = name;
                    phoneUpdate = userPhone;
                    Utilities.saveString(HomeActivity.this, "userPhone", userPhone);
                    tvPhoneNumber.setText(userPhone);
                    user_name.setText(name);
                    edName.setText(nameUpdate);


                } else {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<UserResponseModel> call, Throwable t) {
                dialog0.dismiss();
                t.printStackTrace();
                Toast.makeText(HomeActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void showUpdatedialogue(String profileImagess) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        Dialog dialog;
        dialog = new Dialog(HomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.update_profile_dialog);
        dialog.getWindow().setLayout((int) (width * 0.8), (int) (height * 0.82)); //Controlling width and height.
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button bUpdate = dialog.findViewById(R.id.bUpdate);
        ImageView imgclose = dialog.findViewById(R.id.imgclose);
        iv_profile = dialog.findViewById(R.id.iv_profile);
        edName = dialog.findViewById(R.id.edName);
        EditText ed_Phone = dialog.findViewById(R.id.ed_Phone);
        TextView tvSelectPhoto = dialog.findViewById(R.id.tvSelectPhoto);
        edName.setText(nameUpdate);
        ed_Phone.setText(phoneUpdate);
        if (!TextUtils.isEmpty(profileImage)) {
            Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG + imageUrl).placeholder(R.drawable.user_ic).into(iv_profile);
        }
        bUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getName = edName.getText().toString();
                String getPhonne = ed_Phone.getText().toString();
                if (!isChange) {
                    if (!getName.equals("")) {
                        updateProfileApiNoImage(getName, getPhonne);

                    } else {
                        Toast.makeText(HomeActivity.this, "Name required", Toast.LENGTH_SHORT).show();
                    }
                } else {
//                    input = input_profile_image;
                    if (!TextUtils.isEmpty(getName)) {
                        updateProfileApiWithImage(getName, input, getPhonne);

                    } else {

                        Toast.makeText(HomeActivity.this, "Name required", Toast.LENGTH_SHORT).show();
                    }
                }
//                } else {
//
//                    Toast.makeText(HomeActivity.this, "Profile Image required", Toast.LENGTH_SHORT).show();
//                }
            }
        });
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tvSelectPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doProfilePhoto();
            }
        });
        iv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doProfilePhoto();
            }
        });


        dialog.show();
    }

    private void showdialogueForHelp() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        Dialog dialog;
        dialog = new Dialog(HomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.help_dialog);
        dialog.getWindow().setLayout((int) (width * 0.8), (int) (height * 0.92)); //Controlling width and height.
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView tvPhoneNumber = dialog.findViewById(R.id.tvPhoneNumber);
        ImageView imgclose = dialog.findViewById(R.id.imgclose);
        TextView tvSupportEmaul = dialog.findViewById(R.id.tvSupportEmaul);
        TextView tvPhoneNumberrr = dialog.findViewById(R.id.tvPhoneNumber);
        ImageView tvCopy = dialog.findViewById(R.id.tvCopy);
        tvSupportEmaul.setText(support_email);
        tvPhoneNumberrr.setText(support_phone);
        tvCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                String text;
                myClip = ClipData.newPlainText("text", tvPhoneNumber.getText().toString());
                myClipboard.setPrimaryClip(myClip);

                Toast.makeText(HomeActivity.this, "Phone Number Copied", Toast.LENGTH_SHORT).show();
            }
        });
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }

}