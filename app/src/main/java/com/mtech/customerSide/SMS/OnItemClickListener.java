package com.mtech.customerSide.SMS;

import android.view.View;

public interface OnItemClickListener {

    void onItemClick(View item, int position);
}
