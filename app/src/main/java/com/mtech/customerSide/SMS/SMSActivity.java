package com.mtech.customerSide.SMS;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mtech.customerSide.R;
import com.mtech.customerSide.networks.RetrofitClientInstance;
import com.mtech.customerSide.notifications.MessageNotificationData;
import com.mtech.customerSide.notifications.Notifications;
import com.mtech.customerSide.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;


public class SMSActivity extends AppCompatActivity implements View.OnClickListener,
        OnItemClickListener, SeekBar.OnSeekBarChangeListener {
    TextView title;
    private RecyclerView rv;
    private List<Message> list = new ArrayList<>();
    private Sms_Adapter cs;
    RecyclerView.LayoutManager manager;
    CircleImageView user_profile;
    TextView user_name, user_last_seen, postName, reciever_name_txt;
    ImageView send_sms, ivBack;
    String messageNode = "";
    String currnet_user_id = "";
    String current_user_image = "";
    String current_useR_name = "";
    String reciever_user_id = "";
    String user_profile_image = "";
    String reciever_name = "", reciever_image = "", device_token, message, owner_name;

    Context context;
    ProgressBar progressBar;

    private FirebaseDatabase mDatabase;
    boolean is_loaded_first_time = false;
    EmojiconEditText emojiconEditText, emojiconEditText2;
    ImageView emojiButton;
    View bottom_layout;
    EmojIconActions emojIcon;
    private static final String LOG_TAG = SMSActivity.class.getSimpleName();
    String requestBody = "";
    String reciever_token = "";
    ImageView back_ic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_m_s);
        FirebaseApp.initializeApp(SMSActivity.this);

        init();
        loadChats();

        bottom_layout = findViewById(R.id.bottom_layout);
        emojiButton = (ImageView) findViewById(R.id.emoji_btn);
        emojiconEditText = (EmojiconEditText) findViewById(R.id.emojicon_edit_text);
        emojiconEditText2 = (EmojiconEditText) findViewById(R.id.emojicon_edit_text2);
        emojIcon = new EmojIconActions(this, bottom_layout, emojiconEditText, emojiButton);
        emojIcon.ShowEmojIcon();
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                Log.e("Keyboard", "open");
            }

            @Override
            public void onKeyboardClose() {
                Log.e("Keyboard", "close");
            }
        });


        emojIcon.addEmojiconEditTextList(emojiconEditText2);
        emojiconEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    onSendButtonClicked();
//                    sendNotification();
                    handled = true;
                }
                return handled;
            }
        });


    }

    private void init() {
        back_ic=findViewById(R.id.back_ic);
        back_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        reciever_name_txt = findViewById(R.id.reciever_name);
        send_sms = findViewById(R.id.send_sms);
        rv = (RecyclerView) findViewById(R.id.sms_recyclerview);
        list = new ArrayList<>();

        context = SMSActivity.this;
        currnet_user_id = Utilities.getString(SMSActivity.this, Utilities.USER_ID);
        current_useR_name = Utilities.getString(SMSActivity.this, Utilities.USER_FNAME);
        current_user_image = Utilities.getString(SMSActivity.this, Utilities.USER_PROFILE_IMAGE);

        SharedPreferences sharedPreferences = getSharedPreferences("device_token_social", Context.MODE_PRIVATE);
        device_token = sharedPreferences.getString("device_token", "");

        reciever_token = Utilities.getString(SMSActivity.this, "rec_token");
        reciever_user_id = "1";
        reciever_name = "Admin";
        reciever_image = Utilities.getString(SMSActivity.this, "artist_image");

        mDatabase = FirebaseDatabase.getInstance();
        progressBar = findViewById(R.id.progressBar);
        getToken();


        reciever_name_txt.setText(reciever_name);
        send_sms.setOnClickListener(SMSActivity.this);
        messageNode = getMessagesNode();

        manager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(manager);
        cs = new Sms_Adapter(list, context, SMSActivity.this, messageNode);
        rv.setAdapter(cs);
    }


    private void loadChats() {


        if (!is_loaded_first_time) {
            progressBar.setVisibility(View.VISIBLE);
        }

        if (!messageNode.isEmpty()) {
            DatabaseReference messageQuery = mDatabase.getReference()
                    .child(Database.NODE_MESSAGES).child(messageNode);


            messageQuery.limitToLast(30).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        list.clear();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Message message = snapshot.getValue(Message.class);
                            message.setKey(snapshot.getKey());
                            list.add(message);
                        }
                        rv.getAdapter().notifyDataSetChanged();
                        rv.smoothScrollToPosition(cs.getItemCount() - 1);

                    }

                    if (!is_loaded_first_time) {
                        progressBar.setVisibility(View.GONE);
                    }
                    is_loaded_first_time = true;


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(SMSActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    if (!is_loaded_first_time) {
                        progressBar.setVisibility(View.GONE);
                    }
                }

            });

            messageQuery.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {


                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }


    }


    private String getMessagesNode() {
        String messageNode = null;
        String sendingUID = currnet_user_id;
        String receivingUID = reciever_user_id;
        messageNode = Constants.getMessageNode(sendingUID, receivingUID);

        return messageNode;
    }


    @Override
    public void onClick(View view) {

        int id = view.getId();
        switch (id) {
            case R.id.send_sms:
                onSendButtonClicked();
//                sendNotification();
                break;

        }
    }

    private void onSendButtonClicked() {
        message = emojiconEditText.getText().toString();

        if (!message.isEmpty()) {
            emojiconEditText.setText("");
            writeTextMessage(message, "text");
            Notifications.sendNotification(
                    new MessageNotificationData(reciever_user_id, reciever_name, reciever_image, message),
                    reciever_user_id, this);

        }

    }

    private void writeTextMessage(String data, String type) {


        Message message = new Message();
        message.setSenderUid(currnet_user_id);
        message.setReceiverUid(reciever_user_id);
        message.setType(type);
        message.setData(data);
        message.setToken(reciever_token);
        message.setImage(current_user_image);
        message.setIs_read("false");
        String messagesNode = getMessagesNode();
        String messageNode = mDatabase.getReference().child(Database.NODE_MESSAGES).child(messagesNode).push().getKey();
        mDatabase.getReference().child(Database.NODE_MESSAGES).child(messagesNode).child(messageNode).setValue(message);


        UserChat userChat = new UserChat();
        switch (type) {

            case "audio":
                userChat.setLastMessage("audio");
                break;

            default:
                userChat.setLastMessage(data);
                break;
        }

        userChat.setLastMessageTime(getcurrent_time());


        // add user chat in sending user
        userChat.setUser_image(current_user_image);
        userChat.setUid(current_useR_name);
        userChat.setUnique_key(currnet_user_id);
        userChat.setToken(device_token);
        userChat.setRead("false");
        mDatabase.getReference().child(Database.NODE_USER_CHATS).child(reciever_user_id).child(currnet_user_id).setValue(userChat);


        // add user chat in receiving user
        userChat.setUser_image(reciever_image);
        userChat.setUid(reciever_name);
        userChat.setUnique_key(reciever_user_id);
        userChat.setToken(reciever_token);
        userChat.setRead("true");
        mDatabase.getReference().child(Database.NODE_USER_CHATS).child(currnet_user_id).child(reciever_user_id).setValue(userChat);

    }

    private String getcurrent_time() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(c);

    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onItemClick(View item, int position) {

    }

    public class Sms_Adapter extends RecyclerView.Adapter<Sms_Adapter.MySmsViewHolder> {
        public List<Message> adapter_list;
        public String currnet_user = "";
        public String messageNode = "";
        public Context context;
        public OnItemClickListener onItemClickListener;
        public DatabaseReference messageQuery;

        @Override
        public MySmsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == 0) {
                View my__sms_view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_sms_item, parent, false);
                MySmsViewHolder viewHolder_my = new MySmsViewHolder(my__sms_view);
                return viewHolder_my;
            } else {

                // 1 mean other sms
                View other_people_sms_view = LayoutInflater.from(parent.getContext()).inflate(R.layout.other_perons_sms_item, parent, false);
                MySmsViewHolder viewHolder_other_people_sms =
                        new MySmsViewHolder(other_people_sms_view);
                return viewHolder_other_people_sms;
            }


        }

        @Override
        public void onBindViewHolder(final MySmsViewHolder holder, final int position) {


            String type = adapter_list.get(position).getType();
            switch (type) {
                case "text":

                    holder.bubble_layout.setVisibility(View.VISIBLE);
                    holder.title.setText(adapter_list.get(position).getData());

                    break;
            }


            Glide.with(getApplicationContext())
                    .load(RetrofitClientInstance.BASE_URL_IMG + adapter_list.get(position).getImage()).placeholder(R.drawable.user_ic)
                    .into(holder.cv);

//            Picasso.get().load()
//                    .error(R.drawable.ic_man)
//                    .into(holder.cv);

        }


        private void seen_message(int position) {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("is_read", "true");
            messageQuery.child(adapter_list.get(position).getKey()).updateChildren(hashMap);
        }

        @Override
        public int getItemCount() {
            return adapter_list.size();
        }

        Sms_Adapter(List<Message> list, Context context, OnItemClickListener onItemClickListener, String messageNode) {
            this.adapter_list = list;
            this.onItemClickListener = onItemClickListener;
            this.messageNode = messageNode;
            this.context = context;

            currnet_user = String.valueOf(Utilities.getString(SMSActivity.this, Utilities.USER_ID));

            messageQuery = FirebaseDatabase.getInstance().getReference()
                    .child(Database.NODE_MESSAGES).child(messageNode);


        }


        @Override
        public int getItemViewType(int position) {


            if (messageFromCurrentUser(adapter_list.get(position).getSenderUid())) {
                return 0;
            } else {
                //  seen_message(position);
                return 1;

            }


        }

        private boolean messageFromCurrentUser(String senderUid) {
            if (currnet_user.equalsIgnoreCase(senderUid)) {
                return true;
            }
            return false;
        }

        public class MySmsViewHolder extends RecyclerView.ViewHolder {
            CircleImageView cv;

            TextView title;
            TextView dish;
            LinearLayout bubble_layout;


            public MySmsViewHolder(View itemView) {
                super(itemView);
                cv = (CircleImageView) itemView.findViewById(R.id.user_image);
                title = (TextView) itemView.findViewById(R.id.msgs);
                bubble_layout = itemView.findViewById(R.id.bubble_layout);


            }


        }

    }

    public void getToken() {

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference().child("tokens").child(reciever_user_id);

//        DatabaseReference messageQuery = mDatabase.getReference()
//                .child("typing_Status").child(reciever_user_id);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {


                Iterable<DataSnapshot> data = snapshot.getChildren();

                for (DataSnapshot d : data) {
                    if (d.getKey().equals("device_token")) {
                        reciever_token = d.getValue().toString();


                    }
                }


//                for (DataSnapshot dataSnapshot: snapshot.getChildren()){
//
//                    String status = ""+dataSnapshot.child("typing_to").getValue();
//                    if (status.equals(currnet_user_id)){
//
//                        Toast.makeText(context, "Typing", Toast.LENGTH_SHORT).show();
//
//                    }else {
//
//                        Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
//
//                    }
//                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }


}