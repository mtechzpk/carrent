package com.mtech.customerSide.SMS;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtech.customerSide.R;
import com.mtech.customerSide.networks.RetrofitClientInstance;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdaptor extends RecyclerView.Adapter<ChatAdaptor.ChatHolder> {

    List<UserChat> list;
    OnItemClickListener onItemClickListener;
    Context context;

    public ChatAdaptor(List<UserChat> list, OnItemClickListener onItemClickListene, Context context) {
        this.list = list;
        this.onItemClickListener = onItemClickListene;
        this.context = context;
    }

    @NonNull
    @Override
    public ChatHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_chat_item, viewGroup, false);
        ChatHolder viewHolder = new ChatHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ChatHolder chatHolder, final int i) {

        chatHolder.user_name.setText(list.get(i).getUid());
        String message = list.get(i).getLastMessage();

        if (message.contains("image")) {

        } else {

            chatHolder.user_message.setText(list.get(i).getLastMessage());
        }

        try {

            String image = list.get(i).getUser_image();

            Glide.with(context)
                    .load(RetrofitClientInstance.BASE_URL_IMG +image)
                    .thumbnail(0.25f)
                    .into(chatHolder.image);

//            Picasso.get().load(list.get(i).getUser_image())
//                    .error(R.drawable.ic_man)
//                    .into(chatHolder.image);

        } catch (Exception error) {
            error.printStackTrace();
        }



            if (list.get(i).getRead().equals("true")) {

//            chatHolder.un_read_count.setVisibility(View.GONE);

        } else {
//            chatHolder.un_read_count.setVisibility(View.VISIBLE);

        }
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        try {
            Date date = inputFormat.parse(list.get(i).getLastMessageTime());
            PrettyTime p = new PrettyTime();
            long millis = date.getTime();
            String datetime = p.format(new Date(millis));
            chatHolder.time.setText(datetime);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        chatHolder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(v, i);
            }
        });

        chatHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(v, i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ChatHolder extends RecyclerView.ViewHolder {
        public CircleImageView image;
        public ImageView un_read_count;
        public TextView user_name, user_message, time;


        public ChatHolder(@NonNull View itemView) {
            super(itemView);
//            un_read_count = itemView.findViewById(R.id.un_read_count);
            image = itemView.findViewById(R.id.iv_profile);
            user_name = (TextView) itemView.findViewById(R.id.tv_name);
            user_message = itemView.findViewById(R.id.tv_last_msg);
            time = itemView.findViewById(R.id.tv_time);

        }
    }
}
