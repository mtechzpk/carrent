package com.mtech.customerSide.networks;

import com.mtech.customerSide.model.CommonResponseModel;
import com.mtech.customerSide.model.UserResponseModel;
import com.mtech.customerSide.model.booking.AcceptedBookingResponseModel;
import com.mtech.customerSide.model.booking.BookingResponseModel;
import com.mtech.customerSide.model.home.CarLsitResponseModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface GetDateService {

    //Register User
    @Headers({
            "Accept: application/json",
    })

    @FormUrlEncoded
    @POST("signup")
    Call<UserResponseModel> RegisterUser(@Field("name") String name,
                                         @Field("email") String email,
                                         @Field("phone") String phone,
                                         @Field("password") String password,
                                         @Field("password_confirmation") String password_confirmation
    );

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("signup")
    Call<ResponseBody> signupApi(@Field("name") String name,
                                 @Field("email") String email,
                                 @Field("phone") String phone,
                                 @Field("password") String password,
                                 @Field("password_confirmation") String password_confirmation
    );
    //Login User

    @Headers({
            "Accept: application/json",
    })


    @FormUrlEncoded
    @POST("login")
    Call<UserResponseModel> loginUser(@Field("phone") String email,
                                      @Field("password") String password,
                                      @Field("token") String token
    );

    @Headers({
            "Accept: application/json",
    })


    @FormUrlEncoded
    @POST("update_profile_image")
    Call<UserResponseModel> update_profile_image(@Field("user_id") String user_id,
                                                 @Field("image") String image,
                                                 @Field("name") String name,
                                                 @Field("phone") String phone
    );

    @Headers({
            "Accept: application/json",
    })


    @FormUrlEncoded
    @POST("update_profile_image")
    Call<UserResponseModel> update_profile_imageNoImage(@Field("user_id") String user_id,
                                                        @Field("name") String name,
                                                        @Field("phone") String phone
    );


    @Headers({
            "Accept: application/json",
    })


    @FormUrlEncoded
    @POST("admin_login")
    Call<UserResponseModel> admin_login(@Field("email") String email,
                                        @Field("password") String password
    );


    //Update User Profile

    @Headers({
            "Accept: application/json",
    })


    @FormUrlEncoded
    @POST("update_profile")
    Call<UserResponseModel> updateProfile(@Field("name") String name,
                                          @Field("email") String email,
                                          @Field("phone") String phone,
                                          @Field("user_id") String user_id
    );

    @Headers({
            "Accept: application/json",
    })


    @FormUrlEncoded
    @POST("forgot_password")
    Call<CommonResponseModel> forgot_password(@Field("email") String email);

    @Headers({
            "Accept: application/json",
    })


    @FormUrlEncoded
    @POST("check_credentials")
    Call<CommonResponseModel> check_credentials(@Field("email") String email,
                                                @Field("phone") String phone);


    @Headers({
            "Accept: application/json",
    })


    @FormUrlEncoded
    @POST("verify_code")
    Call<CommonResponseModel> verify_code(@Field("email") String email,
                                          @Field("verification_code") String verification_code);


    @Headers({
            "Accept: application/json",
    })


    @FormUrlEncoded
    @POST("reset_password")
    Call<CommonResponseModel> reset_password(@Field("email") String email,
                                             @Field("password") String password,
                                             @Field("confirm_password") String confirm_password);

    @Headers({
            "Accept: application/json",
    })
    @GET("get_all_vehicles")
    Call<CarLsitResponseModel> get_all_vehicles(
    );


    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("my_bookings")
    Call<BookingResponseModel> my_bookings(@Field("customer_id") String customer_id);


    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("my_bookings")
    Call<AcceptedBookingResponseModel> my_bookingss(@Field("customer_id") String customer_id);


    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("book_vehicle")
    Call<CommonResponseModel> book_vehicle(@Field("customer_id") String customer_id,
                                           @Field("vehicle_id") String vehicle_id,
                                           @Field("pickup_date_and_time") String pickup_date_and_time,
                                           @Field("return_date_and_time") String return_date_and_time,
                                           @Field("pickup_location") String pickup_location,
                                           @Field("pickup_latitude") String pickup_latitude,
                                           @Field("pickup_longitude") String pickup_longitude,
                                           @Field("return_location") String return_location,
                                           @Field("return_latitude") String return_latitude,
                                           @Field("return_longitude") String return_longitude,
                                           @Field("payment_method") String payment_method,
                                           @Field("customer_name") String customer_name,
                                           @Field("customer_phone") String customer_phone,
                                           @Field("customer_address") String customer_address,
                                           @Field("driving_license_image") String driving_license_image,
                                           @Field("rental_duration") String rental_duration
    );

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("picked_booking")
    Call<CommonResponseModel> picked_booking(
            @Field("booking_request_id") String booking_request_id);


    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("end_booking")
    Call<CommonResponseModel> end_booking(
            @Field("booking_request_id") String booking_request_id);

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("accept_booking_request")
    Call<CommonResponseModel> accept_booking_request(
            @Field("booking_request_id") String booking_request_id);

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("change_booking_status")
    Call<CommonResponseModel> change_booking_status(
            @Field("booking_request_id") String booking_request_id,
            @Field("booking_status") String booking_status,
            @Field("cancellation_reason") String cancellation_reason);
}
