package com.mtech.customerSide.Chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.mtech.customerSide.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ChatModel> list;

    public ChatAdapter(Context context, ArrayList<ChatModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.single_chat_item,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ChatModel model = list.get(position);
        holder.Profile.setImageResource(model.getProfile());
        holder.tv_Time.setText(model.getTime());
        holder.tv_Name.setText(model.getName());
        holder.tv_last_msg.setText(model.getLast_msg());


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private CircleImageView Profile;
        private TextView tv_Time,tv_Name,tv_last_msg;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            Profile = itemView.findViewById(R.id.iv_profile);
            tv_Time = itemView.findViewById(R.id.tv_time);
            tv_Name = itemView.findViewById(R.id.tv_name);
            tv_last_msg = itemView.findViewById(R.id.tv_last_msg);
        }
    }
}
