package com.mtech.customerSide.Chat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mtech.customerSide.R;
import com.mtech.customerSide.SMS.ChatAdaptor;
import com.mtech.customerSide.SMS.Database;
import com.mtech.customerSide.SMS.OnItemClickListener;
import com.mtech.customerSide.SMS.SMSActivity;
import com.mtech.customerSide.SMS.UserChat;
import com.mtech.customerSide.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

public class InboxActivity extends AppCompatActivity implements OnItemClickListener {

    private RecyclerView chat_recyclerView;
    private ArrayList<ChatModel> chatList;
    private ChatAdapter chatAdapter;


    List<UserChat> list = new ArrayList<>();
    Context context;
    String currnet_user = "";
    RelativeLayout empty_msg;
    ProgressBar progress_bar;
    LinearLayout contextView;
    DatabaseReference reference;
ImageView ic_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);

        chat_recyclerView = findViewById(R.id.chat_recyclerview);

        progress_bar = findViewById(R.id.progressBar);
        ic_back = findViewById(R.id.ic_back);
        empty_msg = findViewById(R.id.empty_msg);

        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
//        initChatData();

        currnet_user = String.valueOf(Utilities.getString(InboxActivity.this, Utilities.USER_ID));
        getUserChats();
        chat_recyclerView.setAdapter(new ChatAdaptor(list, this, context));

        getUserChats();
    }


    private void getUserChats() {

        progress_bar.setVisibility(View.VISIBLE);
        reference = FirebaseDatabase.getInstance().getReference().child(
                Database.NODE_USER_CHATS).child(
                currnet_user);
        reference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    list.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        UserChat message = snapshot.getValue(UserChat.class);
                        message.setKey(snapshot.getKey());
                        list.add(message);
                    }

                    chat_recyclerView.getAdapter().notifyDataSetChanged();
                    if (list.size() > 0) {
                        empty_msg.setVisibility(View.GONE);
                    } else {
                        empty_msg.setVisibility(View.VISIBLE);

                    }
                    progress_bar.setVisibility(View.GONE);
                } else {
                    if (list.size() > 0) {
                        empty_msg.setVisibility(View.GONE);
                    } else {
                        empty_msg.setVisibility(View.VISIBLE);

                    }
                    progress_bar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                progress_bar.setVisibility(View.GONE);

            }

        });


    }

    @Override
    public void onItemClick(View item, int position) {

        String owner_id = list.get(position).getUnique_key();
        String owner_name = list.get(position).getUid();
        String owner_token = list.get(position).getToken();
        String image = list.get(position).getUser_image();

        Utilities.saveString(InboxActivity.this, "artist_id", owner_id);
        Utilities.saveString(InboxActivity.this, "artist_name", owner_name);
        Utilities.saveString(InboxActivity.this, "rec_token", owner_token);
        Utilities.saveString(InboxActivity.this, "artist_image", image);

        Intent chat_intent = new Intent(InboxActivity.this, SMSActivity.class);
        startActivity(chat_intent);

        progress_bar.setVisibility(View.GONE);
    }
}