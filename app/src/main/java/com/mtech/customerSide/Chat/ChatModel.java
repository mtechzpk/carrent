package com.mtech.customerSide.Chat;

public class ChatModel {

    private Integer profile;
    private String time,name,last_msg;

    public ChatModel(Integer profile, String time, String name, String last_msg) {
        this.profile = profile;
        this.time = time;
        this.name = name;
        this.last_msg = last_msg;
    }

    public ChatModel() {
    }

    public Integer getProfile() {
        return profile;
    }

    public void setProfile(Integer profile) {
        this.profile = profile;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_msg() {
        return last_msg;
    }

    public void setLast_msg(String last_msg) {
        this.last_msg = last_msg;
    }
}
