package com.mtech.customerSide;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "MyCart")
public class Cart {
    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "Carname")
    public String Carname;
    @ColumnInfo(name = "car_price")
    public String car_price;
    @ColumnInfo(name = "car_description")
    public String car_description;
    @ColumnInfo(name = "pickup_location")
    public String pickup_location;
    @ColumnInfo(name = "return_location")
    public String return_location;
    @ColumnInfo(name = "return_dateTime")
    public String return_dateTime;
    @ColumnInfo(name = "pickupDateTime")
    public String pickupDateTime;
    @ColumnInfo(name = "payment_method")
    public String payment_method;

    public Cart() {
    }

    public Cart(String carname, String car_price, String car_description, String pickup_location, String return_location, String return_dateTime, String pickupDateTime, String payment_method) {
        Carname = carname;
        this.car_price = car_price;
        this.car_description = car_description;
        this.pickup_location = pickup_location;
        this.return_location = return_location;
        this.return_dateTime = return_dateTime;
        this.pickupDateTime = pickupDateTime;
        this.payment_method = payment_method;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCarname() {
        return Carname;
    }

    public void setCarname(String carname) {
        Carname = carname;
    }

    public String getCar_price() {
        return car_price;
    }

    public void setCar_price(String car_price) {
        this.car_price = car_price;
    }

    public String getCar_description() {
        return car_description;
    }

    public void setCar_description(String car_description) {
        this.car_description = car_description;
    }

    public String getPickup_location() {
        return pickup_location;
    }

    public void setPickup_location(String pickup_location) {
        this.pickup_location = pickup_location;
    }

    public String getReturn_location() {
        return return_location;
    }

    public void setReturn_location(String return_location) {
        this.return_location = return_location;
    }

    public String getReturn_dateTime() {
        return return_dateTime;
    }

    public void setReturn_dateTime(String return_dateTime) {
        this.return_dateTime = return_dateTime;
    }

    public String getPickupDateTime() {
        return pickupDateTime;
    }

    public void setPickupDateTime(String pickupDateTime) {
        this.pickupDateTime = pickupDateTime;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }
}
