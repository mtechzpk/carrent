package com.mtech.customerSide.uiadapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mtech.customerSide.sidenavigationFragments.AcceptedBookingFragment;
import com.mtech.customerSide.sidenavigationFragments.CurrentFragment;
import com.mtech.customerSide.sidenavigationFragments.PastFragment;

public class CurrentMainTabAdapter extends FragmentStatePagerAdapter {
    public CurrentMainTabAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new CurrentFragment ();
            case 1:
                return new AcceptedBookingFragment();

        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Pending";
            case 1:
                return "Accepted";
            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

}
