package com.mtech.customerSide.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtech.customerSide.R;
import com.mtech.customerSide.model.home.VehicleSpecifciationModel;
import com.mtech.customerSide.networks.RetrofitClientInstance;

import java.util.List;

public class SpecificationAdapter extends RecyclerView.Adapter<SpecificationAdapter.ViewHolder> {


    private Context context;
    private List<VehicleSpecifciationModel> list;
    int category_id;
    String cat_name,cat_image;



    public SpecificationAdapter(Context context, List<VehicleSpecifciationModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_car_spec,parent,false);
        return  new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        VehicleSpecifciationModel model = list.get(position);
        holder.tv_carName.setText(model.getSpecification_name ());
        holder.tvCapacity.setText(model.getSpecification_value ());
        Glide.with(context).load(RetrofitClientInstance.BASE_URL_IMG+model.getSpecification_icon ()).into(holder.img_picture);
        holder.ll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                cat_name = model.getCategory_name();
//                category_id= model.getId();
//                cat_image=model.getCategory_image();
//                Intent intent = new Intent(context, CarDetailsActivity.class);
//                Gson gson = new Gson ();
//                String mymaincategoryobject = gson.toJson(model);
//                intent.putExtra ("model",mymaincategoryobject);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                Utilities.saveInt(context,"category_id",category_id);
////                Utilities.saveString(context,"cat_name",cat_name);
////                Utilities.saveString(context,"cat_image",cat_image);
//                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public  class  ViewHolder extends RecyclerView.ViewHolder{

        TextView tvCapacity,tv_carName;
        LinearLayout ll_item;
        ImageView img_picture;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);



            img_picture = itemView.findViewById(R.id.img_picture);
            tv_carName = itemView.findViewById(R.id.tv_carName);
            tvCapacity = itemView.findViewById(R.id.tvCapacity);
            ll_item =itemView.findViewById(R.id.ll_item);
//            cat_names =itemView.findViewById(R.id.text_cat);

        }
    }
}

