package com.mtech.customerSide.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtech.customerSide.R;
import com.mtech.customerSide.activities.CarDetailsActivity;
import com.mtech.customerSide.model.CarTypesModel;
import com.mtech.customerSide.model.home.VehicleImageModel;
import com.mtech.customerSide.model.home.VehicleSpecifciationModel;
import com.mtech.customerSide.networks.RetrofitClientInstance;
import com.mtech.customerSide.utils.Utilities;

import java.util.List;

public class CarTypesAdapter extends RecyclerView.Adapter<CarTypesAdapter.ViewHolder> {


    private Context context;
    private List<CarTypesModel> list;
    int category_id;
    String rate = "";
    Double  rateperDay;
    String cat_name, cat_image;
    private List<VehicleImageModel> vehicleImageModels;
    private List<VehicleSpecifciationModel> vehicleSpecifciationModels;

    public CarTypesAdapter(Context context, List<CarTypesModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_bookied_vehicle, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CarTypesModel model = list.get(position);
        holder.tv_carName.setText(model.getVehicle_name());
         rateperDay = Double.parseDouble(model.getPer_hour_rate()) * 24;
        rate = model.getPer_hour_rate_currency() + String.format("%.0f", rateperDay) + "/" + " Day";
        holder.tv_price.setText(rate);
        if (list.get(position).getVehicle_images() != null && list.get(position).getVehicle_images().size() >= 1) {

            String image = list.get(position).getVehicle_images().get(0).getVehicle_image();
            if (!image.equals("") && !image.equals("null") && image != null) {
                Glide.with(context).load(RetrofitClientInstance.BASE_URL_IMG + image).placeholder(R.drawable.placeholder).into(holder.img_picture);
                Utilities.saveString(context, "vehicle_image", image);

            }

        }

//        Glide.with(context).load(RetrofitClientInstance.BASE_URL_IMG+image).into(holder.img_picture);
        holder.ll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rateperDay = Double.parseDouble(model.getPer_hour_rate()) * 24;
                vehicleImageModels = list.get(position).getVehicle_images();
                vehicleSpecifciationModels = list.get(position).getVehicle_specifications();
                Utilities.setVehicleImageModel(context, vehicleImageModels);
                Utilities.setVehicleSpecificationModels(context, vehicleSpecifciationModels);
                Utilities.saveString(context, "carDescription", model.getVehicle_description());
                Utilities.saveString(context, "vehicle_color", model.getVehicle_color());
                Utilities.saveString(context, "vehicle_Name", model.getVehicle_name());
                Utilities.saveString(context, "carRate", model.getPer_hour_rate());
                Utilities.saveString(context, "carRateperDay", String.format("%.0f", rateperDay));
                Utilities.saveString(context, "carRateWithCurrency", rate);
                Utilities.saveString(context, "currency", model.getPer_hour_rate_currency());
                Utilities.saveString(context, "vehicle_id", String.valueOf(model.getId()));

                context.startActivity(new Intent(context, CarDetailsActivity.class).putExtra("position", position));


            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_price, tv_carName;
        LinearLayout ll_item;
        ImageView img_picture;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            img_picture = itemView.findViewById(R.id.img_picture);
            tv_carName = itemView.findViewById(R.id.tv_carName);
            tv_price = itemView.findViewById(R.id.tv_price);
            ll_item = itemView.findViewById(R.id.ll_item);
//            cat_names =itemView.findViewById(R.id.text_cat);

        }
    }
}
