package com.mtech.customerSide.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;


import com.mtech.customerSide.Cart;
import com.mtech.customerSide.MyDatabase;
import com.mtech.customerSide.R;

import java.util.ArrayList;


public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.CategoryCardViewHolder> {

    Context context;
    ArrayList<Cart> list;

    public InvoiceAdapter(Context context, ArrayList<Cart> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public CategoryCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_invoice_list, parent, false);
        return new CategoryCardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryCardViewHolder holder, int position) {

        Cart current = list.get(position);
        holder.tv_priceperhour.setText(current.getCar_price());
//        holder.tv_carName.setText(current.getCarname()+" ("+current.getColor()+")");
        holder.tv_carName.setText(current.getCarname());
        holder.tv_description.setText(current.getCar_description());
        holder.tv_pickupdatetime.setText(current.getPickupDateTime());
        holder.tv_pickuplocation.setText(current.getPickup_location());
        holder.tv_returnlocation.setText(current.getReturn_location());
        holder.tv_returndatetime.setText(current.getReturn_dateTime());
        holder.tv_paymentmethod.setText(current.getPayment_method());
        holder.deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Successfully Deleted", Toast.LENGTH_SHORT).show();
                MyDatabase database = Room.databaseBuilder(context, MyDatabase.class, "MyCart").allowMainThreadQueries().build();
                int id = list.get(position).getId();
                database.cartDao().deleteItem(id);
                list.remove(position);
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CategoryCardViewHolder extends RecyclerView.ViewHolder {
        TextView tv_carName, tv_priceperhour,tv_paymentmethod, tv_description, tv_pickuplocation, tv_pickupdatetime, tv_returnlocation,
                tv_returndatetime;
        ImageView deleteItem;

        public CategoryCardViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_priceperhour = itemView.findViewById(R.id.tv_priceperhour);
            tv_carName = itemView.findViewById(R.id.tv_carName);
            tv_description = itemView.findViewById(R.id.tv_description);
            tv_pickupdatetime = itemView.findViewById(R.id.tv_pickupdatetime);
            tv_pickuplocation = itemView.findViewById(R.id.tv_pickuplocation);
            tv_returnlocation = itemView.findViewById(R.id.tv_returnlocation);
            tv_returndatetime = itemView.findViewById(R.id.tv_returndatetime);
            deleteItem = itemView.findViewById(R.id.deleteItem);
            tv_paymentmethod = itemView.findViewById(R.id.tv_paymentmethod);
            tv_pickuplocation.setSelected(true);
            tv_returnlocation.setSelected(true);
        }
    }

}
