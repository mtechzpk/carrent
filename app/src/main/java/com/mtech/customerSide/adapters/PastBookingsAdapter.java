package com.mtech.customerSide.adapters;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mtech.customerSide.R;
import com.mtech.customerSide.activities.BookingsDetialsActivity;
import com.mtech.customerSide.model.booking.PastBookingDataModel;
import com.mtech.customerSide.networks.RetrofitClientInstance;
import com.mtech.customerSide.utils.Utilities;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class PastBookingsAdapter extends RecyclerView.Adapter<PastBookingsAdapter.ViewHolder> {

    String rate = "";
    Double rateperDay;
    private Context context;
    private List<PastBookingDataModel> list;
    int category_id;
    String cat_name, cat_image;


    public PastBookingsAdapter(Context context, List<PastBookingDataModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.pastbookingsitem, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PastBookingDataModel model = list.get(position);

        String date = list.get(position).getCreated_at();
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
//        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
//        SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM, yyyy hh:mm:ss");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM, yyyy");
        Date parsedDate = null;
        try {
            parsedDate = inputFormat.parse(date);
            String formattedDate = outputFormat.format(parsedDate);
            System.out.println(formattedDate);
            holder.tv_date.setText(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        holder.tv_carName.setText(model.getVehicle_name());
        holder.tv_state.setText(model.getStatus());
        rateperDay = Double.parseDouble(model.getPer_hour_rate()) * 24;
        rate = model.getPer_hour_rate_currency() + String.format("%.0f", rateperDay) + "/" + " Day";

        holder.tv_price.setText(rate);

        if (!TextUtils.isEmpty(model.getVehicle_image())){
            Glide.with(context).load(RetrofitClientInstance.BASE_URL_IMG+model.getVehicle_image ()).into(holder.img_picture);

        }else{
            holder.img_picture.setBackgroundResource(R.drawable.placeholder);
        }


//        Glide.with(context).load(model.getImg_Url ())
//                .centerInside()
//                .into(holder.img_picture);

        holder.ll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.saveString(context, "bookType","past");
                rateperDay = Double.parseDouble(model.getPer_hour_rate()) * 24;
                Utilities.saveString(context, "carRateperDay", String.format("%.0f", rateperDay));
                Utilities.setPastBookingModels(context, list);
                context.startActivity(new Intent(context, BookingsDetialsActivity.class).putExtra("position", position));


            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_price, tv_carName, tv_date,tv_state;
        LinearLayout ll_item;
        ImageView img_picture;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_picture = itemView.findViewById(R.id.img_picture);
            tv_carName = itemView.findViewById(R.id.tv_carName);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_date = itemView.findViewById(R.id.tv_date);
            ll_item = itemView.findViewById(R.id.ll_item);
            tv_state = itemView.findViewById(R.id.tv_state);
//            cat_names =itemView.findViewById(R.id.text_cat);

        }
    }
}


