package com.mtech.customerSide.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtech.customerSide.R;
import com.mtech.customerSide.activities.BookingsDetialsActivity;
import com.mtech.customerSide.model.booking.CurrentBookingDataModel;
import com.mtech.customerSide.networks.RetrofitClientInstance;
import com.mtech.customerSide.utils.Utilities;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CurrentBookingsAdapter extends RecyclerView.Adapter<CurrentBookingsAdapter.ViewHolder> {


    private Context context;
    private List<CurrentBookingDataModel> list;
    int category_id;
    String rate = "";
    Double rateperDay;
    String cat_name, cat_image;
    private List<CurrentBookingDataModel> currentBookingDataModels;


    public CurrentBookingsAdapter(Context context, List<CurrentBookingDataModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_bookings, parent, false);
        return new ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CurrentBookingDataModel model = list.get(position);
        holder.tv_carName.setText(model.getVehicle_name());

        rateperDay = Double.parseDouble(model.getPer_hour_rate()) * 24;
        rate = model.getPer_hour_rate_currency() + String.format("%.0f", rateperDay) + "/" + " Day";

        holder.tv_price.setText(rate);
        holder.tv_state.setText(model.getStatus());
        Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG + model.getVehicle_image()).placeholder(R.drawable.placeholder).into(holder.img_picture);
        //Glide.with(context).load(model.getImg_Url ()).into(holder.img_picture);


//        Glide.with(context).load(model.getImg_Url ())
//                .centerInside()
//                .into(holder.img_picture);

        holder.ll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rateperDay = Double.parseDouble(model.getPer_hour_rate()) * 24;

                Utilities.saveString(context, "carRateperDay", String.format("%.0f", rateperDay));
                Utilities.saveString(context, "bookType", "pending");
                Utilities.setCurrentBookingModels(context, list);
                context.startActivity(new Intent(context, BookingsDetialsActivity.class).putExtra("position", position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_price, tv_carName, tv_state;
        LinearLayout ll_item;
        ImageView img_picture;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            img_picture = itemView.findViewById(R.id.img_picture);
            tv_carName = itemView.findViewById(R.id.tv_carName);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_state = itemView.findViewById(R.id.tv_state);
            ll_item = itemView.findViewById(R.id.ll_item);
//            cat_names =itemView.findViewById(R.id.text_cat);

        }
    }
}

