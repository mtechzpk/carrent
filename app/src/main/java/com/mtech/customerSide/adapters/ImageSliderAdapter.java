package com.mtech.customerSide.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.makeramen.roundedimageview.RoundedImageView;
import com.mtech.customerSide.R;
import com.mtech.customerSide.model.home.VehicleImageModel;
import com.mtech.customerSide.networks.RetrofitClientInstance;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImageSliderAdapter extends RecyclerView.Adapter<ImageSliderAdapter.ViewHolder> {

    private Context context;
    private List<VehicleImageModel> list;
    ViewPager2 viewPager2;

    public ImageSliderAdapter(Context context, List<VehicleImageModel> list, ViewPager2 viewPager2) {
        this.context = context;
        this.list = list;
        this.viewPager2 = viewPager2;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.design_image_slider_container, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        VehicleImageModel model = list.get(position);
        Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG+model.getVehicle_image()).into(holder.image);

        if (position == list.size() - 2) {

            viewPager2.post(runnable);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.slider_image);
        }

        void setImage(VehicleImageModel imageSliderModel) {
            Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG+imageSliderModel.getVehicle_image()).into(image);
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {

            list.addAll(list);
            notifyDataSetChanged();
        }
    };
}


