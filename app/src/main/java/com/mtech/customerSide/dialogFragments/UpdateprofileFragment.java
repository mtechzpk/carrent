//package com.mtech.customerSide.dialogFragments;
//
//import android.app.FragmentManager;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.text.TextUtils;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import androidx.fragment.app.DialogFragment;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.NetworkError;
//import com.android.volley.NoConnectionError;
//import com.android.volley.ParseError;
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.StringRequest;
//import com.google.android.gms.location.LocationRequest;
//import com.kaopiz.kprogresshud.KProgressHUD;
//import com.mtech.customerSide.R;
//import com.mtech.customerSide.activities.HomeActivity;
//import com.mtech.customerSide.networks.RetrofitClientInstance;
//import com.mtech.customerSide.utils.Utilities;
//import com.squareup.picasso.Picasso;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.HashMap;
//import java.util.Map;
//
//
//public class UpdateprofileFragment extends DialogFragment {
//    KProgressHUD hud;
//    private LocationRequest locationRequest;
//    Bitmap selected_img_bitmap;
//    String input = "";
//    ImageView iv_profile;
//    String utilityName = "", image = "", profileImage = "", input_profile_image = "", userPhone = "";
//
//    private EditText edName, ed_AccountName, ed_AccountNumber;
//    private Button btn_updatebankDetails;
//    String accessToken = "";
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View v = inflater.inflate(R.layout.update_profile_dialog, container, false);
//        if (getDialog() != null && getDialog().getWindow() != null) {
//            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        }
//        utilityName = Utilities.getString(getActivity(), "userName");
//        profileImage = Utilities.getString(getActivity(), Utilities.USER_PROFILE_IMAGE);
//        userPhone = Utilities.getString(getActivity(), "userPhone");
//        Button bUpdate = v.findViewById(R.id.bUpdate);
//        ImageView imgclose = v.findViewById(R.id.imgclose);
//        iv_profile = v.findViewById(R.id.iv_profile);
//        edName = v.findViewById(R.id.edName);
//        EditText ed_Phone = v.findViewById(R.id.ed_Phone);
//        TextView tvSelectPhoto = v.findViewById(R.id.tvSelectPhoto);
//        ed_Phone.setText(userPhone);
//        ed_Phone.setEnabled(false);
//        edName.setText(utilityName);
//        if (!TextUtils.isEmpty(profileImage)) {
//            Picasso.get().load(RetrofitClientInstance.BASE_URL_IMG + profileImage).placeholder(R.drawable.user_ic).into(iv_profile);
//
//
//        }
//        bUpdate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String getName = edName.getText().toString();
//                if (input.equals("")) {
//                    if (!getName.equals("")) {
//                        updateProfileApiNoImage(getName);
//
//                    } else {
//                        Toast.makeText(getContext(), "Name required", Toast.LENGTH_SHORT).show();
//                    }
//                } else if (!input.equals("")) {
////                    input = input_profile_image;
//                    if (!TextUtils.isEmpty(getName)) {
//                        updateProfileApiWithImage(getName, input);
//                    } else {
//
//                        Toast.makeText(getContext(), "Name required", Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//
//                    Toast.makeText(getContext(), "Image Required", Toast.LENGTH_SHORT).show();
//                }
////                } else {
////
////                    Toast.makeText(getActivity(), "Profile Image required", Toast.LENGTH_SHORT).show();
////                }
//            }
//        });
//        imgclose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//        tvSelectPhoto.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                doProfilePhoto();
//            }
//        });
//        iv_profile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                doProfilePhoto();
//            }
//        });
//
//
//        return v;
//    }
//
//    public interface ItemClickListener {
//        void onItemClick(String item);
//    }
//
//    public static interface OnCompleteListener {
//        public abstract void onComplete(String time);
//    }
//
//    public void show(FragmentManager manager) {
//    }
//
//
//}