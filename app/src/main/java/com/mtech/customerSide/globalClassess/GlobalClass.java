package com.mtech.customerSide.globalClassess;

import android.app.Application;


import com.mtech.customerSide.model.booking.AcceptedBookingDataModel;
import com.mtech.customerSide.model.booking.CurrentBookingDataModel;
import com.mtech.customerSide.model.booking.PastBookingDataModel;
import com.mtech.customerSide.model.home.VehicleImageModel;
import com.mtech.customerSide.model.home.VehicleSpecifciationModel;

import java.util.List;

public class GlobalClass extends Application {
    private List<VehicleImageModel> vehicleImagesModels;
    private List<VehicleSpecifciationModel> vehicleSpecifciationModels;
    private List<AcceptedBookingDataModel> acceptedBookingDataModels;

    private List<CurrentBookingDataModel> currentBookingDataModels;
    private List<PastBookingDataModel> pastBookingDataModels;

    public List<PastBookingDataModel> getPastBookingDataModels() {
        return pastBookingDataModels;
    }
    public void setPastBookingDataModels(List<PastBookingDataModel> pastBookingDataModels) {
        this.pastBookingDataModels = pastBookingDataModels;
    }

    public List<CurrentBookingDataModel> getCurrentBookingDataModels() {
        return currentBookingDataModels;
    }

    public void setCurrentBookingDataModels(List<CurrentBookingDataModel> currentBookingDataModels) {
        this.currentBookingDataModels = currentBookingDataModels;
    }

    public List<VehicleImageModel> getVehicleImagesModels() {
        return vehicleImagesModels;
    }

    public void setVehicleImagesModels(List<VehicleImageModel> vehicleImagesModels) {
        this.vehicleImagesModels = vehicleImagesModels;
    }

    public List<VehicleSpecifciationModel> getVehicleSpecifciationModels() {
        return vehicleSpecifciationModels;
    }

    public void setVehicleSpecifciationModels(List<VehicleSpecifciationModel> vehicleSpecifciationModels) {
        this.vehicleSpecifciationModels = vehicleSpecifciationModels;
    }

    public List<AcceptedBookingDataModel> getAcceptedBookingDataModels() {
        return acceptedBookingDataModels;
    }

    public void setAcceptedBookingDataModels(List<AcceptedBookingDataModel> acceptedBookingDataModels) {
        this.acceptedBookingDataModels = acceptedBookingDataModels;
    }
}
