package com.mtech.customerSide.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.mtech.customerSide.globalClassess.GlobalClass;
import com.mtech.customerSide.model.booking.AcceptedBookingDataModel;
import com.mtech.customerSide.model.booking.CurrentBookingDataModel;
import com.mtech.customerSide.model.booking.PastBookingDataModel;
import com.mtech.customerSide.model.home.VehicleImageModel;
import com.mtech.customerSide.model.home.VehicleSpecifciationModel;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Utilities {

    static ProgressDialog dialog;
    private Context context;

    public Utilities(Context context){
        this.context = context;
    }
    public static final String MAP_KEY = "AIzaSyDqCH1HTTLdcchBvgZQf2nAw-lFZ4sXkjE";

    public static final String USER_ID = "userID";
    public static final String USER_FNAME = "userName";
    public static final String USER_PROFILE_IMAGE = "profileImage";
    public static final String USER_TOKEN = "user_token";
    public static void showProgressDialog(Context ctx, String msg) {
        dialog = new ProgressDialog(ctx);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(msg);
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public static void hideProgressDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.cancel();
            dialog = null;
        }

    }

    public static void makeToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void saveInt(Context context, String key, int value) {
        SharedPreferences sharedPref = context.getSharedPreferences("CarRentSharedStorage", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, value);
        editor.apply();


    }

    public static int getInt(Context context, String key) {

        SharedPreferences sharedPref = context.getSharedPreferences("CarRentSharedStorage", Context.MODE_PRIVATE);
        int val=sharedPref.getInt(key,0);
        return val;

    }

    public static void saveString(Context context, String key, String value) {
        SharedPreferences sharedPref = context.getSharedPreferences("CarRentSharedStorage", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();

    }
    public static void saveArray(Context context, String key, String[] value) {
        SharedPreferences sharedPref = context.getSharedPreferences("CarRentSharedStorage", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value[0]);
        editor.apply();

    }

    public static String getString(Context context, String key) {

        SharedPreferences sharedPref = context.getSharedPreferences("CarRentSharedStorage", Context.MODE_PRIVATE);
        String val=sharedPref.getString(key,"");

        return val;

    }

    public static String getArray(Context context, String[] key) {

        SharedPreferences sharedPref = context.getSharedPreferences("CarRentSharedStorage", Context.MODE_PRIVATE);
        String val=sharedPref.getString(key[0],"");

        return val;

    }
    public static void clearSharedPref(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("CarRentSharedStorage", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.clear();
        editor.apply();



    }

    public static void hideKeyboard(View view, Context context) {
        InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

    public static String changeDateFormate(String date, String from, String to){

        String finalDate= date;

//        SimpleDateFormat date_formate = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat date_formate = new SimpleDateFormat(from);

        Date datee = null;
        try {
            datee = date_formate.parse(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

//        SimpleDateFormat df = new SimpleDateFormat("MMM-dd-yyyy");
        SimpleDateFormat df = new SimpleDateFormat(to);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(datee);
        finalDate = df.format(calendar.getTime());

        return finalDate;

    }
    public static BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public static String getAddress(Context context, LatLng latLng) {

        if (latLng == null) {
            return "";
        }
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = new ArrayList<>();

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 3);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!addresses.isEmpty())
            return addresses.get(0).getAddressLine(0);
        else
            return "";
    }
    public static String differenceBetweenDated(String dates_formate, String dateEnd, String dateStr){

        String days = "";

        SimpleDateFormat sdf = new SimpleDateFormat(dates_formate);

        try {
            Date dateS = sdf.parse(dateStr);
            Date dateE = sdf.parse(dateEnd);

            long diff = dateE.getTime() - dateS.getTime();


            days = String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));

        } catch (ParseException e) {
            e.printStackTrace();
        }



        return days;

    }

    public static String getCurrentDate(){

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = mdformat.format(calendar.getTime());

        return currentDate;

    }

    public static String formattTwoDecimal(Context context, int number){


        if (number == 0){

            return String.valueOf("RS 0.00");
        }
        else {

            String qa = String.valueOf(number);
            String nocomma = qa.replace(",", "");
            Float f = Float.parseFloat(nocomma);
            String.format("%.2f",f);
            DecimalFormat formatter = new DecimalFormat("#,###,###.00#");
            String yourFormattedString = formatter.format(f);

            return String.valueOf("RS "+yourFormattedString);
        }

    }



    public static boolean isNetworkConnected(Context mcon) {
        ConnectivityManager cm = (ConnectivityManager) mcon.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }


    public static  void setVehicleImageModel(Context context, List<VehicleImageModel> stylistServicesModels) {
        GlobalClass globalClass = (GlobalClass) context.getApplicationContext();
        globalClass.setVehicleImagesModels(stylistServicesModels);
    }

    public static List<VehicleImageModel> getVehicleImageModel(Context context) {
        GlobalClass globalClass = (GlobalClass) context.getApplicationContext();
        return  globalClass.getVehicleImagesModels();

    }



    public static  void setVehicleSpecificationModels(Context context, List<VehicleSpecifciationModel> vehicleSpecifciationModels) {
        GlobalClass globalClass = (GlobalClass) context.getApplicationContext();
        globalClass.setVehicleSpecifciationModels(vehicleSpecifciationModels);
    }

    public static List<VehicleSpecifciationModel> getVehicleSpecificationModels(Context context) {
        GlobalClass globalClass = (GlobalClass) context.getApplicationContext();
        return  globalClass.getVehicleSpecifciationModels();

    }
    public static  void setCurrentBookingModels(Context context, List<CurrentBookingDataModel> currentBookingDataModels) {
        GlobalClass globalClass = (GlobalClass) context.getApplicationContext();
        globalClass.setCurrentBookingDataModels(currentBookingDataModels);
    }

    public static List<CurrentBookingDataModel> getCurrentBookingModels(Context context) {
        GlobalClass globalClass = (GlobalClass) context.getApplicationContext();
        return  globalClass.getCurrentBookingDataModels();

    }

    public static  void setAcceptedBookingModels(Context context, List<AcceptedBookingDataModel> currentBookingDataModels) {
        GlobalClass globalClass = (GlobalClass) context.getApplicationContext();
        globalClass.setAcceptedBookingDataModels(currentBookingDataModels);
    }

    public static List<AcceptedBookingDataModel> getAcceptedBookingModels(Context context) {
        GlobalClass globalClass = (GlobalClass) context.getApplicationContext();
        return  globalClass.getAcceptedBookingDataModels();

    }

    public static  void setPastBookingModels(Context context, List<PastBookingDataModel> pastBookingDataModels) {
        GlobalClass globalClass = (GlobalClass) context.getApplicationContext();
        globalClass.setPastBookingDataModels(pastBookingDataModels);
    }

    public static List<PastBookingDataModel> getPastBookingModels(Context context) {
        GlobalClass globalClass = (GlobalClass) context.getApplicationContext();
        return  globalClass.getPastBookingDataModels();

    }


}
