package com.mtech.customerSide.sidenavigationFragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.customerSide.R;
import com.mtech.customerSide.adapters.AcceptedBookingsAdapter;
import com.mtech.customerSide.adapters.CurrentBookingsAdapter;
import com.mtech.customerSide.model.booking.AcceptedBookingDataModel;
import com.mtech.customerSide.model.booking.BookingResponseModel;
import com.mtech.customerSide.model.booking.CurrentBookingDataModel;
import com.mtech.customerSide.networks.GetDateService;
import com.mtech.customerSide.networks.RetrofitClientInstance;
import com.mtech.customerSide.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AcceptedBookingFragment extends Fragment {
    private RecyclerView category_recyclerview;
    private AcceptedBookingsAdapter adpter;
    TextView tvStatus;
    private LinearLayoutManager manager;
    List<AcceptedBookingDataModel> models;
    View v;
    KProgressHUD dialog0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_current, container, false);
        String userID = Utilities.getString(getContext(), Utilities.USER_ID);

        init(v);
        getCurrentBookingApi(userID);

        return v;
    }

    private void init(View v) {
        models = new ArrayList<>();
        category_recyclerview = v.findViewById(R.id.recyclerview);
        tvStatus = v.findViewById(R.id.tvStatus);
    }

    private void getCurrentBookingApi(String userId) {

        dialog0 = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<BookingResponseModel> call = service.my_bookings(userId);
        call.enqueue(new Callback<BookingResponseModel>() {
            @Override
            public void onResponse(Call<BookingResponseModel> call, Response<BookingResponseModel> response) {
                assert response.body() != null;
                int status = response.body().getStatus();
                if (response.body() != null) {
                    models = response.body().getData().getAcceptedBookingDataModels();
                    if (status == 200) {
                        tvStatus.setVisibility(View.GONE);

                        dialog0.dismiss();
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        dialog0.dismiss();
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        tvStatus.setVisibility(View.VISIBLE);
                    }
                    setData(category_recyclerview, models);
                } else {
                    tvStatus.setVisibility(View.VISIBLE);

                    Toast.makeText(getContext(), "result not found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BookingResponseModel> call, Throwable t) {

                dialog0.dismiss();
                t.printStackTrace();
            }
        });

    }

    private void setData(RecyclerView rvAvailableDate, List datalist) {

        adpter = new AcceptedBookingsAdapter(getContext(), datalist);
        GridLayoutManager manager = new GridLayoutManager(getContext(), 1, RecyclerView.VERTICAL, false);
        rvAvailableDate.setHasFixedSize(true);
        rvAvailableDate.setLayoutManager(manager);
        rvAvailableDate.setAdapter(adpter);
    }
}