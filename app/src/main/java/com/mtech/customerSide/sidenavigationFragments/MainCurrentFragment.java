package com.mtech.customerSide.sidenavigationFragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.material.tabs.TabLayout;
import com.mtech.customerSide.R;
import com.mtech.customerSide.uiadapters.BookingsTabAdapter;
import com.mtech.customerSide.uiadapters.CurrentMainTabAdapter;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

public class MainCurrentFragment extends Fragment {
    TabLayout tabLayout;
    ViewPager viewPagerHomeTabs;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_main_current, container, false);
        init();
        return view;
    }

    private void init() {

        tabLayout = view.findViewById(R.id.home_tabLayout);
        viewPagerHomeTabs = view.findViewById(R.id.viewPagerHomeTabs);
        final CurrentMainTabAdapter myPagerAdapter = new CurrentMainTabAdapter
                (getChildFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPagerHomeTabs.setAdapter(myPagerAdapter);
        tabLayout.setupWithViewPager(viewPagerHomeTabs);
        setMarginBetweenTabs(tabLayout);
    }

    public void setMarginBetweenTabs(TabLayout tabLayout) {

        ViewGroup tabs = (ViewGroup) tabLayout.getChildAt(0);
        for (int i = 0; i < tabs.getChildCount() - 1; i++) {
            View tab = tabs.getChildAt(i);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            layoutParams.setMarginEnd(-1);
            tab.setLayoutParams(layoutParams);
            tabLayout.requestLayout();
        }
    }

    @Override
    public void onResume() {
        init();
        super.onResume();
    }
}