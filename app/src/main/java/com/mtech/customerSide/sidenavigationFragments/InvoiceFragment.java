package com.mtech.customerSide.sidenavigationFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;


import com.mtech.customerSide.Cart;
import com.mtech.customerSide.MyDatabase;
import com.mtech.customerSide.R;
import com.mtech.customerSide.adapters.InvoiceAdapter;

import java.util.ArrayList;

public class InvoiceFragment extends Fragment {

    View view;
    InvoiceAdapter adapterCategories;
    ArrayList<Cart> carts;
    RecyclerView rvCategory;
    LinearLayoutManager linearLayoutManager;
    TextView tvFav;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_invoice, container, false);
        rvCategory = view.findViewById(R.id.rvCategory);
        tvFav = view.findViewById(R.id.tvFav);

        MyDatabase database = Room.databaseBuilder(getActivity(), MyDatabase.class, "MyCart").allowMainThreadQueries().build();
        carts = (ArrayList<Cart>) database.cartDao().getAllCartItem();
        if (carts.isEmpty()) {
            tvFav.setVisibility(View.VISIBLE);
        }
        rvCategory.setHasFixedSize(true);
        linearLayoutManager = new GridLayoutManager(getContext(), 1, LinearLayoutManager.VERTICAL, false);
        rvCategory.setLayoutManager(linearLayoutManager);

        adapterCategories = new InvoiceAdapter(getActivity(), carts);

        rvCategory.setAdapter(adapterCategories);

        return view;
    }
}