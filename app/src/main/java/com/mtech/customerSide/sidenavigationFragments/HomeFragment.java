package com.mtech.customerSide.sidenavigationFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mtech.customerSide.R;
import com.mtech.customerSide.adapters.CarTypesAdapter;
import com.mtech.customerSide.model.CarTypesModel;
import com.mtech.customerSide.model.home.CarLsitResponseModel;
import com.mtech.customerSide.networks.GetDateService;
import com.mtech.customerSide.networks.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    private RecyclerView category_recyclerview;
    private CarTypesAdapter adpter;
    private StaggeredGridLayoutManager manager;
    View v;
    TextView tvStatus;
    KProgressHUD dialog0;
    private List<CarTypesModel> carTypesModels;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_home, container, false);

        init(v);
        getVehicleListApi();

        return v;
    }

    private void init(View v) {
        carTypesModels = new ArrayList<>();
        category_recyclerview = v.findViewById(R.id.category_recyclerview);
        tvStatus = v.findViewById(R.id.tvStatus);
//        progressHUD = KProgressHUD.create(this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setWindowColor(getResources().getColor(R.color.colorBlack))
//                .setAnimationSpeed(3)
//                .setDimAmount(0.5f)
//                .setCancellable(true)
//                .setLabel("Please Wait")
//                .setCornerRadius(10)
//                .show();
    }

    private void getVehicleListApi() {

        dialog0 = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


        GetDateService service = RetrofitClientInstance.getRetrofitInstance().create(GetDateService.class);
        Call<CarLsitResponseModel> call = service.get_all_vehicles();
        call.enqueue(new Callback<CarLsitResponseModel>() {
            @Override
            public void onResponse(Call<CarLsitResponseModel> call, Response<CarLsitResponseModel> response) {

                assert response.body() != null;
                int status = response.body().getStatus();
                carTypesModels = response.body().getData();
                if (carTypesModels != null && !carTypesModels.isEmpty()) {
                    dialog0.dismiss();
                    if (status == 200) {
                        dialog0.dismiss();
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        dialog0.dismiss();
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    setData(category_recyclerview, carTypesModels);
                    tvStatus.setVisibility(View.GONE);

                } else {
                    dialog0.dismiss();
                    tvStatus.setVisibility(View.VISIBLE);
                    Toast.makeText(getContext(), "no vehicle found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CarLsitResponseModel> call, Throwable t) {

                dialog0.dismiss();
                t.printStackTrace();
            }
        });

    }

    private void setData(RecyclerView rvAvailableDate, List datalist) {

        adpter = new CarTypesAdapter(getContext(), datalist);
        GridLayoutManager manager = new GridLayoutManager(getContext(),2,RecyclerView.VERTICAL,false);
        rvAvailableDate.setHasFixedSize(true);
        rvAvailableDate.setLayoutManager(manager);
        rvAvailableDate.setAdapter(adpter);
    }
}