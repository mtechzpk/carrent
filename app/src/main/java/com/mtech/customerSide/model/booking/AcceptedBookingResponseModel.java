package com.mtech.customerSide.model.booking;

import com.google.gson.annotations.SerializedName;

public class AcceptedBookingResponseModel {

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private AcceptedBookingDataModel data;

    public AcceptedBookingResponseModel(int status, String message, AcceptedBookingDataModel data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AcceptedBookingDataModel getData() {
        return data;
    }

    public void setData(AcceptedBookingDataModel data) {
        this.data = data;
    }
}
