package com.mtech.customerSide.model.home;

import com.google.gson.annotations.SerializedName;
import com.mtech.customerSide.model.CarTypesModel;

import java.util.List;

public class CarLsitResponseModel {

    @SerializedName("status")
    private int status;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private List<CarTypesModel> data;

    public CarLsitResponseModel(int status, String message, List<CarTypesModel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CarTypesModel> getData() {
        return data;
    }

    public void setData(List<CarTypesModel> data) {
        this.data = data;
    }
}
