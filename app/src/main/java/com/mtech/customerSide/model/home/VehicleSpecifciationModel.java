package com.mtech.customerSide.model.home;

import com.google.gson.annotations.SerializedName;

public class VehicleSpecifciationModel {
    @SerializedName("id")
    private int id;

    @SerializedName("specification_icon")
    private String specification_icon;
    @SerializedName("specification_name")
    private String specification_name;
    @SerializedName("specification_value")
    private String specification_value;

    public VehicleSpecifciationModel(int id, String specification_icon, String specification_name, String specification_value) {
        this.id = id;
        this.specification_icon = specification_icon;
        this.specification_name = specification_name;
        this.specification_value = specification_value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSpecification_icon() {
        return specification_icon;
    }

    public void setSpecification_icon(String specification_icon) {
        this.specification_icon = specification_icon;
    }

    public String getSpecification_name() {
        return specification_name;
    }

    public void setSpecification_name(String specification_name) {
        this.specification_name = specification_name;
    }

    public String getSpecification_value() {
        return specification_value;
    }

    public void setSpecification_value(String specification_value) {
        this.specification_value = specification_value;
    }
}

