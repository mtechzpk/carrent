package com.mtech.customerSide.model.home;

import com.google.gson.annotations.SerializedName;

public class VehicleImageModel {
    @SerializedName("id")
    private int id;

    @SerializedName("vehicle_image")
    private String vehicle_image;

    public VehicleImageModel(int id, String vehicle_image) {
        this.id = id;
        this.vehicle_image = vehicle_image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVehicle_image() {
        return vehicle_image;
    }

    public void setVehicle_image(String vehicle_image) {
        this.vehicle_image = vehicle_image;
    }
}

