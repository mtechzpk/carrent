package com.mtech.customerSide.model.booking;

import com.google.gson.annotations.SerializedName;

public class BookingResponseModel {

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private BookingDataModel data;


    public BookingResponseModel(int status, String message, BookingDataModel data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BookingDataModel getData() {
        return data;
    }

    public void setData(BookingDataModel data) {
        this.data = data;
    }
}
