package com.mtech.customerSide.model;

public class SpecificationsModel {
    String specificationName,specificationPrice,image;

    public SpecificationsModel() {
    }

    public SpecificationsModel(String specificationName, String specificationPrice, String image) {
        this.specificationName = specificationName;
        this.specificationPrice = specificationPrice;
        this.image = image;
    }

    public String getSpecificationName() {
        return specificationName;
    }

    public void setSpecificationName(String specificationName) {
        this.specificationName = specificationName;
    }

    public String getSpecificationPrice() {
        return specificationPrice;
    }

    public void setSpecificationPrice(String specificationPrice) {
        this.specificationPrice = specificationPrice;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
