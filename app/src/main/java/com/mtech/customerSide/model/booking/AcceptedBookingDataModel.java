package com.mtech.customerSide.model.booking;

import com.google.gson.annotations.SerializedName;

public class AcceptedBookingDataModel {
    @SerializedName("id")
    private int id;
    @SerializedName("vehicle_id")
    private int vehicle_id;
    @SerializedName("pickup_date_and_time")
    private String pickup_date_and_time;
    @SerializedName("return_date_and_time")
    private String return_date_and_time;
    @SerializedName("pickup_location")
    private String pickup_location;
    @SerializedName("pickup_latitude")
    private String pickup_latitude;
    @SerializedName("pickup_longitude")
    private String pickup_longitude;

    @SerializedName("vehicle_description")
    private String vehicle_description;

    @SerializedName("return_location")
    private String return_location;
    @SerializedName("return_latitude")
    private String return_latitude;

    @SerializedName("return_longitude")
    private String return_longitude;
    @SerializedName("payment_method")
    private String payment_method;
    @SerializedName("status")
    private String status;
    @SerializedName("vehicle_image")
    private String vehicle_image;

    @SerializedName("per_hour_rate")
    private String per_hour_rate;
    @SerializedName("per_hour_rate_currency")
    private String per_hour_rate_currency;

    @SerializedName("vehicle_name")
    private String vehicle_name;

    @SerializedName("rental_duration")
    private String rental_duration;

    public AcceptedBookingDataModel() {
    }

    public AcceptedBookingDataModel(int id, int vehicle_id, String pickup_date_and_time, String return_date_and_time, String pickup_location, String pickup_latitude, String pickup_longitude, String return_location, String return_latitude, String return_longitude, String payment_method, String status, String vehicle_image, String per_hour_rate, String per_hour_rate_currency, String vehicle_name) {
        this.id = id;
        this.vehicle_id = vehicle_id;
        this.pickup_date_and_time = pickup_date_and_time;
        this.return_date_and_time = return_date_and_time;
        this.pickup_location = pickup_location;
        this.pickup_latitude = pickup_latitude;
        this.pickup_longitude = pickup_longitude;
        this.return_location = return_location;
        this.return_latitude = return_latitude;
        this.return_longitude = return_longitude;
        this.payment_method = payment_method;
        this.status = status;
        this.vehicle_image = vehicle_image;
        this.per_hour_rate = per_hour_rate;
        this.per_hour_rate_currency = per_hour_rate_currency;
        this.vehicle_name = vehicle_name;
    }

    public String getVehicle_image() {
        return vehicle_image;
    }

    public void setVehicle_image(String vehicle_image) {
        this.vehicle_image = vehicle_image;
    }

    public String getPer_hour_rate() {
        return per_hour_rate;
    }

    public void setPer_hour_rate(String per_hour_rate) {
        this.per_hour_rate = per_hour_rate;
    }

    public String getPer_hour_rate_currency() {
        return per_hour_rate_currency;
    }

    public void setPer_hour_rate_currency(String per_hour_rate_currency) {
        this.per_hour_rate_currency = per_hour_rate_currency;
    }

    public String getVehicle_description() {
        return vehicle_description;
    }

    public void setVehicle_description(String vehicle_description) {
        this.vehicle_description = vehicle_description;
    }

    public String getVehicle_name() {
        return vehicle_name;
    }

    public void setVehicle_name(String vehicle_name) {
        this.vehicle_name = vehicle_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(int vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getPickup_date_and_time() {
        return pickup_date_and_time;
    }

    public void setPickup_date_and_time(String pickup_date_and_time) {
        this.pickup_date_and_time = pickup_date_and_time;
    }

    public String getReturn_date_and_time() {
        return return_date_and_time;
    }

    public void setReturn_date_and_time(String return_date_and_time) {
        this.return_date_and_time = return_date_and_time;
    }

    public String getPickup_location() {
        return pickup_location;
    }

    public void setPickup_location(String pickup_location) {
        this.pickup_location = pickup_location;
    }

    public String getPickup_latitude() {
        return pickup_latitude;
    }

    public void setPickup_latitude(String pickup_latitude) {
        this.pickup_latitude = pickup_latitude;
    }

    public String getPickup_longitude() {
        return pickup_longitude;
    }

    public void setPickup_longitude(String pickup_longitude) {
        this.pickup_longitude = pickup_longitude;
    }

    public String getReturn_location() {
        return return_location;
    }

    public void setReturn_location(String return_location) {
        this.return_location = return_location;
    }

    public String getRental_duration() {
        return rental_duration;
    }

    public void setRental_duration(String rental_duration) {
        this.rental_duration = rental_duration;
    }

    public String getReturn_latitude() {
        return return_latitude;
    }

    public void setReturn_latitude(String return_latitude) {
        this.return_latitude = return_latitude;
    }

    public String getReturn_longitude() {
        return return_longitude;
    }

    public void setReturn_longitude(String return_longitude) {
        this.return_longitude = return_longitude;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}