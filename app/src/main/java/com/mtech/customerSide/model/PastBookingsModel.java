package com.mtech.customerSide.model;

public class PastBookingsModel {

    String imgUrl,carName,carPrice,date;


    public PastBookingsModel() {
    }

    public PastBookingsModel(String imgUrl, String carName, String carPrice, String date) {
        this.imgUrl = imgUrl;
        this.carName = carName;
        this.carPrice = carPrice;
        this.date = date;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getCarPrice() {
        return carPrice;
    }

    public void setCarPrice(String carPrice) {
        this.carPrice = carPrice;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
