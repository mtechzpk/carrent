package com.mtech.customerSide.model.booking;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BookingDataModel {
    @SerializedName("current_bookings")
    private List<AcceptedBookingDataModel> acceptedBookingDataModels;
    @SerializedName("past_bookings")
    private List<PastBookingDataModel> past_bookings;

    @SerializedName("pending_bookings")
    private List<CurrentBookingDataModel> pending_booking;

    public List<AcceptedBookingDataModel> getAcceptedBookingDataModels() {
        return acceptedBookingDataModels;
    }

    public void setAcceptedBookingDataModels(List<AcceptedBookingDataModel> acceptedBookingDataModels) {
        this.acceptedBookingDataModels = acceptedBookingDataModels;
    }

    public List<PastBookingDataModel> getPast_bookings() {
        return past_bookings;
    }

    public void setPast_bookings(List<PastBookingDataModel> past_bookings) {
        this.past_bookings = past_bookings;
    }

    public List<CurrentBookingDataModel> getPending_booking() {
        return pending_booking;
    }

    public void setPending_booking(List<CurrentBookingDataModel> pending_booking) {
        this.pending_booking = pending_booking;
    }
}