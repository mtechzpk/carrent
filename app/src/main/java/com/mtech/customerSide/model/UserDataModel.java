package com.mtech.customerSide.model;

import com.google.gson.annotations.SerializedName;

public class UserDataModel {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("profile_image")
    private String profile_image;
    @SerializedName("support_phone")
    private String support_phone;
    @SerializedName("support_email")
    private String support_email;

    @SerializedName("security_deposite_currency")
    private String security_deposite_currency;
    @SerializedName("security_deposite")
    private String security_deposite;

    public UserDataModel(int id, String name, String email, String phone, String profile_image) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.profile_image = profile_image;
    }

    public String getSupport_phone() {
        return support_phone;
    }

    public void setSupport_phone(String support_phone) {
        this.support_phone = support_phone;
    }

    public String getSupport_email() {
        return support_email;
    }

    public void setSupport_email(String support_email) {
        this.support_email = support_email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getSecurity_deposite_currency() {
        return security_deposite_currency;
    }

    public void setSecurity_deposite_currency(String security_deposite_currency) {
        this.security_deposite_currency = security_deposite_currency;
    }

    public String getSecurity_deposite() {
        return security_deposite;
    }

    public void setSecurity_deposite(String security_deposite) {
        this.security_deposite = security_deposite;
    }
}