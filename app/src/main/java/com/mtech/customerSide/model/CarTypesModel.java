package com.mtech.customerSide.model;

import com.google.gson.annotations.SerializedName;
import com.mtech.customerSide.model.home.VehicleImageModel;
import com.mtech.customerSide.model.home.VehicleSpecifciationModel;

import java.util.List;

public class CarTypesModel {

    @SerializedName("id")
    private int id;

    @SerializedName("vehicle_name")
    private String vehicle_name;

    @SerializedName("vehicle_color")
    private String vehicle_color;

    @SerializedName("vehicle_description")
    private String vehicle_description;

    @SerializedName("per_hour_rate")
    private String per_hour_rate;

    @SerializedName("per_hour_rate_currency")
    private String per_hour_rate_currency;

    @SerializedName("vehicle_images")
    private List<VehicleImageModel> vehicle_images;

    @SerializedName("vehicle_specifications")
    private List<VehicleSpecifciationModel> vehicle_specifications;

    public CarTypesModel(int id, String vehicle_name, String vehicle_description, String per_hour_rate, String per_hour_rate_currency, List<VehicleImageModel> vehicle_images, List<VehicleSpecifciationModel> vehicle_specifications) {
        this.id = id;
        this.vehicle_name = vehicle_name;
        this.vehicle_description = vehicle_description;
        this.per_hour_rate = per_hour_rate;
        this.per_hour_rate_currency = per_hour_rate_currency;
        this.vehicle_images = vehicle_images;
        this.vehicle_specifications = vehicle_specifications;
    }

    public List<VehicleSpecifciationModel> getVehicle_specifications() {
        return vehicle_specifications;
    }

    public void setVehicle_specifications(List<VehicleSpecifciationModel> vehicle_specifications) {
        this.vehicle_specifications = vehicle_specifications;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVehicle_name() {
        return vehicle_name;
    }

    public void setVehicle_name(String vehicle_name) {
        this.vehicle_name = vehicle_name;
    }

    public String getVehicle_description() {
        return vehicle_description;
    }

    public void setVehicle_description(String vehicle_description) {
        this.vehicle_description = vehicle_description;
    }

    public String getPer_hour_rate() {
        return per_hour_rate;
    }

    public void setPer_hour_rate(String per_hour_rate) {
        this.per_hour_rate = per_hour_rate;
    }

    public String getPer_hour_rate_currency() {
        return per_hour_rate_currency;
    }

    public void setPer_hour_rate_currency(String per_hour_rate_currency) {
        this.per_hour_rate_currency = per_hour_rate_currency;
    }

    public List<VehicleImageModel> getVehicle_images() {
        return vehicle_images;
    }

    public void setVehicle_images(List<VehicleImageModel> vehicle_images) {
        this.vehicle_images = vehicle_images;
    }

    public String getVehicle_color() {
        return vehicle_color;
    }

    public void setVehicle_color(String vehicle_color) {
        this.vehicle_color = vehicle_color;
    }
}
