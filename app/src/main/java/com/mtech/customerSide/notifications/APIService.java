package com.mtech.customerSide.notifications;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAXpQjTMo:APA91bHGPwLemOibwFfOF_ELNPAYp2Kxr44Xe4IWos_5UfTSljVfR7a8zbkviSfKpzAN0I4QDz6NISjH4hkfwxL17icqw-nPj8z1Hlms61dYTE3sUis1zSfi-3Cb6h6GUWKkJY9T-967"
            }
    )
    @POST("fcm/send")
    Call<MessageResponseModel> sendNotifcation(@Body NotificationSenderModel body);
}