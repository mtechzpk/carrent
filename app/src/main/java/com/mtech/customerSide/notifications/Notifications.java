package com.mtech.customerSide.notifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.RemoteMessage;
import com.mtech.customerSide.R;
import com.mtech.customerSide.SMS.Database;
import com.mtech.customerSide.SMS.SMSActivity;
import com.mtech.customerSide.activities.HomeActivity;
import com.mtech.customerSide.utils.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Notifications {

    private Context context;

    public Notifications(Context context) {
        this.context = context;
    }

    private static final String CHANNEL_ID = "scNotifications";
    private static final String CHANNEL_NAME = "SC Notifications";
    private static final String CHANNEL_DESC = "Sending and Receiving Notifications";


    //This Method will Display Notification and set the Pending Intents
    static void displayNotifications(Context context, RemoteMessage remoteMessage) {


        String title = "", text = "";
        Intent intent = new Intent(context, HomeActivity.class);

//        if (remoteMessage.getData().get("notificationType").equals("chat")) {

        String receiverID = remoteMessage.getData().get("receiverID");
//        String receiverName = remoteMessage.getData().get("receiverName");
        String receiverName = "Admin";

        String receiverImage = remoteMessage.getData().get("receiverImage");
        String message = remoteMessage.getData().get("message");
        intent = new Intent(context, SMSActivity.class);
        title = receiverName;
        text = message;
        Utilities.saveString(context, "artist_id", receiverID);
        Utilities.saveString(context, "artist_name", receiverName);
        Utilities.saveString(context, "artist_image", receiverImage);
//        }


        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                100,
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                        .setPriority(NotificationCompat.PRIORITY_MAX);

        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(context);
        managerCompat.notify(1, builder.build());

    }

    //This Will create a Channel for the Notification. Called after the Successful Login
    public static void createNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(CHANNEL_DESC);
            NotificationManager manager = context.getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }
    }

    // This Overloaded Method will send Notification to Single Target User
    public static void sendNotification(final MessageNotificationData data,
                                        final String user_id, final Context context) {

        Log.d("YAM", "Sending...");

        final ProgressDialog progressDialog = new ProgressDialog(context);
        final APIService apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);

        FirebaseDatabase.getInstance().getReference(Database.TOKENS).child(user_id)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {

                            String fcmToken = dataSnapshot.getValue().toString();
                            Log.d("YAM", "FCM : " + fcmToken);

                            NotificationSenderModel sender = new NotificationSenderModel(data, fcmToken);
                            apiService.sendNotifcation(sender).enqueue(new Callback<MessageResponseModel>() {
                                @Override
                                public void onResponse(Call<MessageResponseModel> call, Response<MessageResponseModel> response) {
                                    if (response.code() == 200) {
                                        Log.d("YAM", "Response : " + response.code());
//                                        Toast.makeText(context, "success", Toast.LENGTH_SHORT).show();

                                    }
                                }

                                @Override
                                public void onFailure(Call<MessageResponseModel> call, Throwable t) {
                                    Toast.makeText(context, "failed", Toast.LENGTH_SHORT).show();

                                }
                            });

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    // This Overloaded Method will send Notification to Single Target User
    public static void sendNotificationViaToken(final MessageNotificationData data,
                                                final String token, final Context context) {

        Log.d("YAM", "Sending...");
        final APIService apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);

        NotificationSenderModel sender = new NotificationSenderModel(data, token);
        apiService.sendNotifcation(sender).enqueue(new Callback<MessageResponseModel>() {
            @Override
            public void onResponse(Call<MessageResponseModel> call, Response<MessageResponseModel> response) {
                if (response.code() == 200) {
                    Log.d("YAM", "Response : " + response.code());
                }
            }

            @Override
            public void onFailure(Call<MessageResponseModel> call, Throwable t) {
            }
        });

    }
}
